#!/usr/bin/python

import sqlite3
import findYear
from string import Template
import cgi
import re
import pickle
import pygraph.algorithms.accessibility as access

global link_Template 
link_Template= Template(\
	"<a href = http://www.chiark.greenend.org.uk/ucgi/~naath/$script"\
	+" title=$title>$text</a>")

global ageTable
ageTable = "(SELECT diedYear-bornYear as age, name,ID,diedYear,bornYear"\
	+" FROM people"\
	+" WHERE bornYear<>0 AND diedYear<>0 AND (diedMonth>bornMonth"\
	+" OR (diedMonth = bornMonth AND (diedDay>=bornDay OR diedDay =0)))"\
        +" UNION"\
       	+" SELECT diedYear-bornYear-1 as age,name,ID,diedYear,bornYear"\
        +" FROM people"\
        +" WHERE bornYear<>0 AND diedYear<>0 AND (diedMonth<bornMonth"\
        +" OR (diedMonth = bornMonth AND (diedDay<bornDay AND diedDay <>0)))"\
        +" )"

global ageNowTable
ageNowTable = "(SELECT strftime('%Y',date('now'))-bornYear as age,"\
	+"  name,ID,diedYear,bornYear"\
        +" FROM people"\
        +" WHERE bornYear<>0  AND (strftime('%m',date('now'))>bornMonth"\
        +" OR (strftime('%m',date('now')) = bornMonth AND"\
	+" (strftime('%d',date('now'))>=bornDay)))"\
        +" UNION"\
        +" SELECT strftime('%Y',date('now'))-bornYear-1 as age,"\
	+" name,ID,strftime('%Y',date('now')),bornYear"\
        +" FROM people"\
        +" WHERE bornYear<>0  AND (strftime('%m',date('now'))<bornMonth"\
        +" OR (strftime('%m',date('now')) = bornMonth"\
	+" AND (strftime('%d',date('now'))<bornDay)))"\
        +" )"

global ageChildTable
ageChildTable = "(SELECT c.bornYear - p.bornYear as age,"\
	+" c.name as cName,c.ID as cID,"\
	+" p.name as pname,p.ID as pID"\
	+" FROM"\
	+" parents INNER JOIN people c"\
	+" ON parents.ID = c.ID"\
	+" INNER JOIN people p"\
	+" ON parents.parentID = p.ID"\
	+" WHERE p.bornYear <>0 AND c.bornYear<>0"\
	+" AND (c.bornMonth>p.bornMonth"\
	+" OR (c.bornMonth = p.bornMonth AND c.bornDay>=p.bornDay))"\
	+" UNION"\
	+" SELECT c.bornYear - p.bornYear-1 as age,c.name,c.ID,p.name,p.ID"\
	+" FROM"\
	+" parents INNER JOIN people c"\
	+" ON parents.ID = c.ID"\
	+" INNER JOIN people p"\
	+" ON parents.parentID = p.ID"\
	+" WHERE p.bornYear <>0 AND c.bornYear<>0"\
	+" AND (c.bornMonth<p.bornMonth"\
	+" OR (c.bornMonth = p.bornMonth AND c.bornDay<p.bornDay))"\
	+" )"

global maleStyles
maleStyles = ['Emperor','King','Duke','Archduke',\
                'Lord','Count','Prince','Baron']
global femaleStyles
femaleStyles = ['Empress','Queen','Duchess','Archduchess',\
                'Lady','Countess','Princess','Baroness']
global pluralStyles
pluralStyles = ['Emperors','Kings','Dukes','Archdukes',\
		'Lords','Counts','Princes','Barons']

global successorStates
successorStates = {'King of the Anglo-Saxons':'King of the English',\
		'King of the English':'King of England',\
		'King of England':'King of Great Britain',\
		'King of Great Britain':\
		'King of the United Kingdom of Great Britain and Ireland',\
		'King of the United Kingdom of Great Britain and Ireland':\
	'King of the United Kingdom of Great Britain and Northern Ireland',\
		'King of the Franks':'King of France'\
		}

def reverse_dictionary(dict):
        rev_dict={}

        for item in dict.items():
                key = item[0]
                value = item[1]
                if value in rev_dict.keys():
       	                a = rev_dict[value]
               	        a.append(key)
                       	rev_dict[value] = a
               	else:
                       	rev_dict[value]=[key]

        return rev_dict


global predeccessorStates
predeccessorStates = reverse_dictionary(successorStates)

def swap_gender(style):
	for i in range(len(maleStyles)):
		m = maleStyles[i]+' '
		f = femaleStyles[i]+' '
		p = pluralStyles[i]+' '
		if re.search(f,style):
			return re.sub(f,m,style)
		elif re.search(m,style):
			return re.sub(m,f,style)
		elif re.search(p,style):
			return re.sub(p,m,style)

	return style
def make_plural(style):
	for i in range(len(maleStyles)):
                m = maleStyles[i]+' '
                f = femaleStyles[i]+' '
                p = pluralStyles[i]+' '
                if re.search(f,style):
                        return re.sub(f,p,style)
                elif re.search(m,style):
                      	return re.sub(m,p,style)
	return style
def make_singular(style):
	for i in range(len(maleStyles)):
		m = maleStyles[i]+' '
		f = femaleStyles[i]+' '
		p = pluralStyles[i] + ' '
		if re.search(p,style):
			return re.sub(p,m,style)
	return style
def add_quotes(s):
	s = str(s)
	return "'"+s+"'"

def run_query(s,t):
	c = make_cursor()
	return c.execute(s,t)

def make_insert(table,fields):

        t = tuple(fields)
        values = '('
        for i in range(len(fields)):
                values = values+'?,'
        values = values[:-1]+')'

        s = 'INSERT INTO '+table+' VALUES'\
                +values

        run_query(s,t)

def print_row(row,newLine):
	out = ''
	for item in row:
		out += str(item)+'|'
	return out[:-1] + newLine

def print_query(s,t,newLine):
	printMe = ''
	for row in run_query(s,t):
		printMe += print_row(row,newLine)		
	return printMe

def is_number(s):
    try:
        float(s)
        return 1
    except ValueError:
        return 0

def print_tagged_query(relationship,s,t,newLine):
	mine = ''
	for row in run_query(s,t):
		mine += print_tagged_name(relationship,row,newLine)
	return mine


def relationship_html(ID,ID2,newLine):
	if newLine=='<br>':
		relationship = ''
 		marriageSelect = 'SELECT Related FROM marriages'\
			+' WHERE ida=? AND idb=?'
        	marriageID = (min(int(ID),int(ID2)),\
			max(int(ID),int(ID2)))
		for row in run_query(marriageSelect,marriageID):
			if row[0]!='0':
				relationship = row[0]
				break
		if relationship =='':
			relationship = common_ancestors(ID,ID2,newLine)[2]
			
		if relationship[-11:] != 'not related':
			script = "ancestors.py?ID=%s&ID2=%s" % (ID,ID2)
			url = link_Template.substitute\
				(script = script,title = "Common ancestors"\
					,text = "Common ancestors")
			return relationship + ' '+url + newLine
		else:
			return relationship + newLine
	else:
		return ''

def find_adjacent(terr,start,stop,id):
	s = "SELECT name,people.id,stopyear"\
	+" FROM people INNER JOIN styles"\
	+" ON people.id = styles.id"\
	+" INNER JOIN styleDecomp"\
	+" ON styles.style = styleDecomp.style"\
	+" WHERE short = ? AND stopyear<=? AND stopyear <>0 AND people.id<>?"\
	+" ORDER BY startyear DESC;"


	t = (terr,start,id)
	myPrevious=[]
	y=0
	for row in run_query(s,t):
		myPrevious = row[0:2]
		y = row[2]
		break

	t = (swap_gender(terr),start,id)
	for row in run_query(s,t):
		if row[2]>y:
			myPrevious = row[0:2]
		break

	u = "SELECT name,people.id,startyear"\
        +" FROM people INNER JOIN styles"\
  	+" ON people.id = styles.id"\
       	+" INNER JOIN styleDecomp"\
        +" ON styles.style = styleDecomp.style"\
        +" WHERE short = ? AND startyear>=? AND startyear <>0 AND people.id<>?"\
        +" ORDER BY startyear;"
		
	v = (terr,stop,id)
	myNext=[]
	y=float('inf')
        for r in run_query(u,v):
		myNext = r[0:2]
		y = r[2]
                break


	v = (swap_gender(terr),stop,id)
	for r in run_query(u,v):
		if r[2]<y:
			myNext = r[0:2]
		break

	return [myPrevious, myNext]

def terr_html(terr,newLine,start,stop):
	if newLine=='<br>':
		myTitle = add_quotes(terr)

		terrURL=re.sub(' ','%20',terr)

		return link_Template.substitute(\
			script = "territory.py?terr="+terrURL, title=myTitle,\
			text = terr)
	else:
		return terr

def name_html(row,html):
	if html=='<br>':
		html=1
	elif html=='\n':
		html=0

	if row[0] == None:
		return row[1]
	elif row[1]==0:
		return row[0]
	else:
		if html==1:
			script = "person.py?ID=%s" %(row[1])
        	        name = row[0]
	                return link_Template.substitute(script = script\
				,title = add_quotes(name),text = name)
		else:
			return "%s,%s" % (row[0],row[1])

def print_people(n):
	if n!=1:
		return 'people'
	else:
		return 'person'

def print_children(n):
	if n>1:
		return 'children'
	else:	
		return 'child'

def print_age_child_count(row,newLine):
	if newLine == '<br>':
		script = "listAge.py?age=%s" % (row[0])
		link = link_Template.substitute(script = \
			script, title = add_quotes(row[0]), text = row[0])

		out = '%s %s had children at age %s %s'\
		 % (row[1],print_people(row[1]),link,newLine)
		return out
	else:
		return print_row(row,newLine)

def print_age_death_count(row,newLine):
	if newLine =='<br>':
		script = "listAgeDeath.py?age=%s" % (row[0])
		if row[0] != 1000000:
			link = link_Template.substitute(script = script,\
				title = add_quotes(row[0]),text = row[0])
			text = 'died at age'
		else:
			link = link_Template.substitute(script = script,\
				title = 'still alive',text = 'still alive')
			text = 'are'
		out = "%s %s %s %s %s " \
			%(row[1],print_people(row[1]),text, link,newLine)
		return out
	else:
		return print_row(row,newLine)

def print_name_count(row,newLine):
	if newLine=='<br>':
		script = "name.py?name=%s" % (row[0])
		link = link_Template.substitute(script =\
			script, title = add_quotes(row[0]),text = row[0])
		return "%s people called %s%s"\
			%(row[1],link, newLine)
	else:
		return print_row(row,newLine)	

def print_children_count(row,newLine):
	out = "%s %s had " % (row[0],print_people(row[0]))

	if newLine == '<br>':
		script = "listChildCount.py?nc="+str(row[1])
		link = link_Template.substitute(script =\
			script, title = add_quotes(row[1]),text = row[1])
	else:
		link = str(row[1])

	out += "%s %s %s" % (link,print_children(row[1]),newLine)

	return out

def print_tagged_name(relationship,row,newLine):
	if row[0]==None:
               	out = relationship + " not yet entered: " + row[1]
		out = "%s not yet entered: %s" % (relationship,row[1])
       	else:
		if newLine == '<br>':
			html = 1
		else:
			html=0
		if relationship =='':
			out = '%s   ' % (name_html(row,html))
		else:
	                out = relationship + ": " + name_html(row,html)
			out = '%s: %s' % (relationship,name_html(row,html))
	return out + newLine

def month_numbers(monthN):
	if monthN == 0:
 		month ='unknown month'
   	elif monthN == 1:
		month ='January'
	elif monthN==2:
		month ='February'
	elif monthN==3:
		month ='March'
	elif monthN==4:
		month ='April'
	elif monthN==5:
		month ='May'
	elif monthN==6:
		month ='June'
	elif monthN==7:
		month ='July'
	elif monthN==8:
		month ='August'
	elif monthN==9:
		month ='September'
	elif monthN==10:
		month ='October'
	elif monthN==11:
		month ='November'
	elif monthN==12:
		month ='December'
	else:
		month = 'Incorrectly entered month ' + str(monthN)
	return month

def ordinal_numbers(number):
	number = int(number)
	out = '%d' % (number)
	if number % 10==1 and number/10 % 10 !=1:
		out +='st'
	elif number % 10==2 and number/10 % 10 !=1:
		out +='nd'
	elif number % 10==3 and number/10 % 10 !=1:
		out +='rd'
	else:
		out +='th'
	return out

def calendar(newLine):
	out = ''

	mLength = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	b = 'born'
	x = 'died'
	fcstart = '<FONT COLOR = "'
	fcmid = '">'
	fcend = '</FONT>'
	dcol = 'red'
	dcstart = fcstart+dcol+fcmid

	for m in range(12):

		n = count_born_on(m+1,0,b)
		o = count_born_on(m+1,0,x)
		p = count_born_in(m+1,b)
		q = count_born_in(m+1,x)

		script = 'birthday.py?date=%d-0' %(m+1)
		d = 0
		url = link_Template.substitute\
                                (script = script,title = month_numbers(m+1),\
                                text = month_numbers(m+1))
		out += "%s %d %s%d%s %d %s%d%s%s" \
			%(url,n,dcstart,o,fcend,p,dcstart,q,fcend,newLine)


		out+="<table>"
		out+="<tr>"
		for d in range(mLength[m]):
			script = 'birthday.py?date=%d-%d' % (m+1,d+1)
			url = link_Template.substitute\
	        		(script = script,title = str(d+1),\
                		text = str(d+1))

		        n = count_born_on(m+1,d+1,b)
			o = count_born_on(m+1,d+1,x)
	
			s = "<td>"
			e = "</td>"
	       		out+="%s%s%s %s%d%s %s%s%s%s%s " %(s,url,e,\
				s,n,e,s,dcstart,o,fcend,e)
			if (d+1)%5 ==0 or (d+1)==mLength[m]:
				out+="</tr>"
				out+="<tr>"
		out+="</table>"
		out+=newLine

	script = 'birthday.py?date=0-0'
        url = link_Template.substitute\
        	(script = script,title = 'unknown',\
                text = 'unkown')

	n = count_born_on(0,0,b)
	o = count_born_on(0,0,x)
	out+="%s %d %s%d%s%s" %(url,n,dcstart,o,fcend,newLine)



	script = 'birthday.py?date=20-0'
        url = link_Template.substitute\
                (script = script,title = 'on their birthday',\
                text = 'on their birthday')
	n = count_born_on(20,0,b)
	out +="%d people died %s%s" %(n,url,newLine)


	s = "select diedYear,count(*)"\
                +" FROM people"\
                +" WHERE diedYear==1000000"\
                +" GROUP BY diedYear;"
        for row in run_query(s,()):
                out += print_age_death_count(row,newLine)

	out+=newLine

	for bd in ('born','died'):
		s = "SELECT "+bd+", count(*) AS n"\
			+" FROM (SELECT "+bd\
			+" FROM people"\
			+" WHERE "+bd+"Month<>0 AND "+bd+"Day<>0)"\
			+" GROUP BY "+bd\
			+" HAVING n>1;"

		t = "SELECT name,id"\
			+" FROM people"\
			+" WHERE "+bd+" = ?;"
	

		out += "On the following days more than one person %s: %s" \
			%(bd,newLine)

		for row in run_query (s,()):
			out+=row[0]+":"+newLine
			for r in run_query(t,(str(row[0]),)):
				out+=name_html(r,newLine)+newLine



	return out

def count_born_on(month,day,bd):
	if month==20:
		s = "SELECT count(*) FROM people"\
			+" WHERE bornMonth==diedMonth"\
			+" AND bornDay==diedDay"\
			+" AND bornMonth<>0"\
			+" AND diedDay<>0;"
		t = ()

	else:
		s = "SELECT count(*) FROM people"\
			+" WHERE "+bd+"Month==?"\
        	        +" AND "+bd+"Day==?"\
			+" AND "+bd+"Year<>1000000;"

		t = (month,day)

	for row in run_query(s,t):
		return row[0]

def count_born_in(month,bd):

	s = "SELECT count(*) FROM people"\
		+" WHERE "+bd+"Month==?"\
		+" AND "+bd+"Year<>1000000;"
	t = (month,)

	for row in run_query(s,t):
		return row[0]

def born_on(date,newLine):
	month = int(date.split('-')[0])
	day = int(date.split('-')[1])

	fcstart = '<FONT COLOR = "'
        fcmid = '">'
        fcend = '</FONT>'
        dcol = 'red'
        dcstart = fcstart+dcol+fcmid


	out = ''


	if month==20:
		n = count_born_on(month,day,'')

		out+="%d %s died on their birthday: %s"\
			%(n, print_people(n),newLine)

		s =  "SELECT name,id,bornYear,diedYear FROM people"\
			+" WHERE bornMonth==diedMonth"\
                	+" AND bornDay==diedDay"\
                	+" AND bornMonth<>0"\
                	+" AND diedDay<>0"\
			+" ORDER BY diedYear-bornYear;"			
		t=()
		out+="these people died on the day they were born:%s"%(newLine)
		header=0
		for row in run_query(s,t):
			if row[2]!=row[3] and header==0:
				header = 1
				out+="%sthese people died on a later birthday:%s"\
					%(newLine,newLine)
			out += "%s  born %d died %d %s" \
			%(name_html(row,newLine),row[2],row[3],newLine)
		return out

	if day!= 0:
		out += '%s %s %s' \
		%(month_numbers(month),ordinal_numbers(day),newLine)
	else:
		out +='%s %s'\
		%(month_numbers(month),newLine)


	for bd in ['born','died']:
		s = "SELECT name,id,"+bd+"Year FROM people"\
			+" WHERE "+bd+"Month==?"\
			+" AND "+bd+"Day==?"\
			+" AND "+bd+"Year<>1000000"\
			+" ORDER BY "+bd+"Year;"

		t = (month,day)
		n = count_born_on(month,day,bd)
		
		if bd=='died':
			out+=dcstart
		if day!=0:
			out+="%s %s %s on this day %s" \
			%(n,print_people(n),bd, newLine)
		elif month!=0:
			out+="%s %s %s in this month on unknown day %s"\
			%(n,print_people(n),bd,newLine)
		else:
			out+="%s %s %s in unknown month on unknown day %s"\
                	%(n,print_people(n),bd,newLine)
		if bd=='died':
			out+=fcend
		for row in run_query(s,t):
			if row[2]==0:
				y = 'unknown year'
			else:
				y = row[2]
			out+="%s in %s %s " %(name_html(row,newLine),\
       	                	y,newLine)
		out+=newLine

	if day==0 and month!=0:
		for bd in ['born','died']:
			s = "SELECT name,id,"+bd+"Year FROM people"\
                        +" WHERE "+bd+"Month==?"\
                        +" AND "+bd+"Year<>1000000"\
                        +" ORDER BY "+bd+"Year;"

	                t = (month,)
	                n = count_born_in(month,bd)	

	                if bd=='died':
        	                out+=dcstart
        	        out+="%s %s %s in this month %s" \
        	                %(n,print_people(n),bd, newLine)
                	if bd=='died':
                        	out+=fcend

                	for row in run_query(s,t):
                        	if row[2]==0:
                                	y = 'unknown year'
                        	else:
                                	y = row[2]
                                out+="%s in %s %s " %(name_html(row,newLine),\
                                        y,newLine)
 	               	out+=newLine
					
	return out

def list_territories(newLine):
	s = "SELECT DISTINCT short"\
	+" FROM styleDecomp"\
	+" ORDER BY short;"

	out = ''
	terrs = []
	for row in run_query(s,()):
		match = 0
		
		for i in range(len(maleStyles)):
			m = maleStyles[i]+' '
			if re.search(m,row[0]):
				match = 1
				break
		if match==1:
			t = row[0]
		else:
			t = swap_gender(row[0])			

		if t not in terrs:
			terrs.append(t)

	for i in range(len(terrs)):
		terrs[i] = make_plural(terrs[i])
	
	for terr in terrs:
		out += terr_html(terr,newLine,0,0) +newLine

	return out

def list_people_parents():
	s = "SELECT name,id"\
		+" FROM people"\
		+" ORDER BY id;"

	output = []
	for row in run_query(s,()):

		ID = row[1]
		[parents, parentIDs,parentNames] = find_parents(ID)
		[spouses,spousesID,spousesNames,sD] = find_spouses(ID)
		
		[self,myID,myName] = find_person(ID)
		output.append([self,parents,spouses])
	return output


def list_people(newLine):
	s = "SELECT name,id,bornyear,fullname,diedYear"\
	+" FROM people"\
	+" ORDER BY bornyear,diedYear;"

	out = ''
	year = 0
	out = out + 'born in unknown year:' +newLine
	for row in run_query(s,()):
		if row[2]!=0 and row[2]/100==0:
			out +='%sborn in 1st century:%s' % (newLine,newLine)

		if row[2]/100!=year/100:
			century = row[2]/100 + 1
			out +='%sborn in %s century: %s'\
			 %(newLine,ordinal_numbers(century),newLine)


		out+="%s %s: " %(name_html(row,newLine),\
			row[3])

		if row[2] == 0: #unknown year

			t = (row[1],) #person ID

			#died
			if row[4]!=0:
				if row[4]!=1000000:
					out += 'died in %s ' %(row[4])
				else:
					out+='still alive '
			#find children
			u = "Select people.bornYear from"\
                		+" people INNER JOIN parents"\
                		+" ON people.ID = parents.ID"\
                		+" WHERE parents.parentID = ?"\
				+ " ORDER BY people.bornYear;"
			
			cYears = []
			for r in run_query(u,t):
				if r[0] != 0:
					cYears.append(r[0])
			if len(cYears)>0:
				out +=' had '+print_children(len(cYears))+\
					' in '
				for y in cYears:
					out+='%s,' % (y)
				out = out[:-1]

			u = "Select styles.startYear, styles.style"\
                                +" FROM people INNER JOIN styles"\
                                +" ON people.ID = styles.ID"\
                                +" WHERE people.ID = ? and"\
                                +" styles.startYear <>0"\
                                +" ORDER BY styles.startYear;"

                        for r in run_query(u,t):
				if r[0] !='0':
					out+=' %s from %s' %(r[1],r[0])
		else:
			if row[4]!=0 and row[4]!=1000000:
				out+= "%s-%s" %(row[2],row[4])
			elif row[4] == 1000000:
				out+="%s-present" %(row[2])
			else:
				out+="%s-?"%(row[2])
		out += newLine
		year = row[2]
	return out

def count_names(newLine):
	s = "SELECT firstName, count(*)"\
   	+" FROM people"\
   	+" GROUP BY firstName"\
   	+" ORDER BY count(*) DESC;"

	out = ''
	for row in run_query(s,()):
		out += print_name_count(row,newLine)

	return out



def count_children(newLine):

	s = "SELECT count(*),nc"\
	+" FROM ("\
	+" SELECT count(*) AS nc"\
	+" FROM parents INNER JOIN people ON parentID = people.ID"\
	+" GROUP BY parentID"\
	+" HAVING parentID <>'?'"\
	+" AND parentID <> '0')"\
	+" GROUP BY nc;"

	out = ''
	for row in run_query(s,()):
		out += print_children_count(row,newLine)
	return out

def parents_with_children(nChildren,newLine):
	s = "SELECT name,parentID"\
	+ " FROM parents"\
	+ " INNER JOIN people"\
	+ " ON parentID = people.ID"\
	+ " GROUP BY parentID"\
	+ " HAVING count(*) = ?"\
	+ " AND parentID <> 0"\
	+ " ORDER BY bornYear;"


	u = "SELECT count(*)"\
	+" FROM  parents INNER JOIN people"\
	+" ON parents.ID = people.ID"\
	+" WHERE parentID = ?"\
	+" AND (diedyear-bornyear>? OR died='present');"

	out = "People who had %s %s:%s" \
		%(nChildren,print_children(nChildren),newLine)

	for row in run_query(s,(nChildren,)):
		out += name_html(row,newLine)
		for r in run_query(u,(row[1],1)):	
			out += " %d  survived infancy" % r[0]
		for r in run_query(u,(row[1],18)):
			out += " and %s survived to adulthood" % r[0]
		out = out +newLine
	
	return out

def search_name(name,newLine):
	s = "SELECT name, ID,fullname,BornYear,DiedYear"\
        +" FROM people"\
        +" WHERE fullname LIKE ? or name LIKE ?"\
	+" ORDER BY BornYear;"

	IDs=[]
	names=[]

	out = 'Names starting with %s:%s' % (name,newLine)
	t = (name + '%',name+'%')
	fullIDs=[]
	for row in run_query(s,t):
		if row[4] == 1000000:
			d = 'present'
		else:
			d = row[4]
		out+='%s %s (%d-%s)%s' %\
			(name_html(row,newLine),row[2],row[3],d,newLine)
		fullIDs.append(row[1])
		names.append(row[0])
		IDs.append(row[1])

        t = ('%' + name + '%','%'+name+'%')
	out += '%sNames containing %s:%s' %(newLine,name,newLine)
        for row in run_query(s,t):
		if row[1] not in fullIDs:
			if row[4]==1000000:
				d = 'present'
			else:
				d = row[4]
			out+='%s %s (%d-%s)%s' %\
	                        (name_html(row,newLine),row[2],\
				row[3],d,newLine)
			names.append(row[0]+','+row[2])
			IDs.append(row[1])
	
	s = '''SELECT name,people.ID,style,fullname,bornYear, diedYear
	FROM people INNER JOIN styles
	ON styles.id = people.id
	WHERE style LIKE ?
	ORDER BY bornYear;'''
	
	out += '%sStyles containing %s:%s' %(newLine,name,newLine)
	t = (t[0],)
	for row in run_query(s,t):
		out +="%s (%s (%d-%d)) %s %s" \
		%(name_html(row,newLine),row[3],row[4],row[5],row[2],newLine)
        return [out,names,IDs]


def people_with_name(name,newLine):
	s = "SELECT name, ID,fullname"\
	+" FROM people"\
	+" WHERE firstname = ?"\
	+" ORDER BY bornYear;"

	out = ''

	t = (name,)

	for row in run_query(s,t):
		out += name_html(row,newLine)+' '+row[2] + newLine

	return out

def count_birth_month(newLine):
	s = "SELECT bornMonth, count(*)"\
		+" FROM people"\
		+" GROUP BY bornMonth"\
		+" ORDER BY bornMonth;"

	t = "SELECT * FROM people WHERE bornMonth = ?;"

	out = ''
	for row in run_query(s,()):
		month = month_numbers(row[0])
		out += "%s:%s%s" %( month,row[1],newLine)

		if row[0]>12:
			u = (row[0],)
			out +=print_query(t,u,newLine)
		
	return out

def count_death_month(newLine):
        s = "SELECT diedMonth, count(*)"\
                +" FROM people"\
                +" GROUP BY diedMonth"\
                +" ORDER BY diedMonth;"

	t = "SELECT * FROM people WHERE diedMonth = ?;"

        out = ''
        for row in run_query(s,()):
                month = month_numbers(row[0])
                out += '%s:%s%s'%(month,row[1], newLine)

		if row[0]>12:
                        u = (row[0],)
                        out +=print_query(t,u,newLine)

        return out

def count_age_at_child(newLine):

	s = "SELECT age,count(*)"\
		+" FROM "+ageChildTable\
		+" GROUP BY age ORDER BY age;"

	out = ''
	for row in run_query(s,()):
		out +=print_age_child_count(row,newLine)

	return out

def people_had_child_at_age(age,newLine):


	s = "SELECT age,cname,cID,pname,pID"\
		+" FROM "+ageChildTable\
		+" WHERE age = ?;"

	t = (int(age),)
	out = 'At age ' + str(age) + ' :'

	for row in run_query(s,t):
		parent = name_html([row[3],row[4]],newLine)
		child = name_html([row[1],row[2]],newLine)
		out += "%s%s had %s" % (newLine,parent,child)
	return out

def count_age_at_death(newLine):

	s = "select diedYear-bornYear as age,count(*)"\
		+" FROM people"\
		+" WHERE diedYear<>0 AND bornYear<>0 AND diedYear<>1000000"\
		+" GROUP BY age;"


	s = "SELECT age,count(*)"\
		+" FROM"\
		+ ageTable\
		+" WHERE diedYear<>1000000"\
		+" GROUP BY age;"

	out=''
	for row in run_query(s,()):
		out += print_age_death_count(row,newLine)

	s = "select diedYear,count(*)"\
		+" FROM people"\
		+" WHERE diedYear==1000000"\
		+" GROUP BY diedYear;"
	for row in run_query(s,()):
		out += print_age_death_count(row,newLine)

	return out

def people_died_at_age(age,newLine):
	

	if age != '1000000':

		s = "SELECT age,name,ID,diedYear,bornYear"\
		+" FROM "\
		+ ageTable\
		+" WHERE age = ?"\
		+" ORDER BY diedYear;"


		t = (int(age),)
		out = 'These people died at age ' +str(age) + ' :'
		for row in run_query(s,t):
			out += newLine+ name_html([row[1],row[2]],newLine)
	else:
		s = "SELECT diedYear, name,ID"\
			+" FROM people"\
			+" WHERE diedYear = 1000000;"
		out = 'These people are still alive:'
		for row in run_query(s,()):
			out+='%s %s' %(newLine,\
				name_html([row[1],row[2]],newLine))

	return out

def all_ancestors(personID,newLine):

	ancestors = [personID]
	allAncestors = [personID]
	trackLevel = [0]
	level = 0

	s = "SELECT name,id FROM people WHERE id=?"
	t = (personID,)

	out=print_tagged_query("Ancestors of ",s,t,newLine)

	names = ['me']
	allAncestors,trackLevel,names = \
		find_ancestors(personID,allAncestors,trackLevel,names,level)
	textArray=[]
	for i in range(len(allAncestors)):
		a = allAncestors[i]
		l = trackLevel[i]

		if a==0:
			text="%s %s %s" %('',\
				names[i],newLine)
		else:
			text=print_tagged_query('',s,\
				(a,),newLine)

		while len(textArray)<l+1:
			textArray.append('')

		textArray[l]+=text

	for i in range(len(textArray)):
		out += parent_level(i,'parent')+':'+newLine
		out+=textArray[i]



	image = "<img src = ancestorGraph.py?id=%s>" %personID
	out +=newLine + image+newLine

	return [out, allAncestors,trackLevel]


def common_ancestors(IDA,IDB,newLine):

		

	names = ''
	nameList=[]
	s  = "SELECT name,id FROM people WHERE id ==?"
	for id in (IDA,IDB):
		t = (id,)
		for row in run_query(s,t):
			names +=name_html(row,newLine)
			nameList.append(name_html(row,newLine))
			if id==IDA:
				names+=' and '

	if len(nameList)<2:
		names+='this person are not related'

		return [names,[],names]

		
	filename = '/u3/naath/familyTreeProject/familyTree/graphedData'
	file = open(filename)
	g = pickle.load(file)
	file.close()

	a1 = set(access.accessibility(g)[int(IDA)])
        a2 = set(access.accessibility(g)[int(IDB)])

	aBoth = a1.intersection(a2)

	if len(aBoth)==0:
		names+=' are not related'
		return [names,{},names]

	mrca = []
	pA = [IDA]
	pB = [IDB]
	lA = [0]
	lB = [0]
	IDsA = [IDA]
	IDsB = [IDB]

	if IDA==IDB:
		related = names[:-4] + 'are the same person'
		tText = ''
		mcra = [IDA]

	else:
		level = 1
		while 1==1:
			nIDsA =[]
			for id in IDsA:
				ps = find_parents(id)[1]
				for p in ps:
					pA.append(p)
					lA.append(level)
					nIDsA.append(p)
			IDsA = nIDsA
			nIDsB = []				
			for id in IDsB:	
				ps = find_parents(id)[1]
				for p in ps:
					pB.append(p)
					lB.append(level)
					nIDsB.append(p)
			IDsB = nIDsB
			a = set(pA)
			b = set(pB)
			common = (a.intersection(b)).difference(set([0]))

			if len(common)>0:
				break
			if len(IDsB)+len(IDsA) == 0:
				break

			level = level + 1
			

		if len(common)==0:
			related = names+' are not related'
			out = related
			cText = ''
		else:

			ca = list(common)
			aL = float('inf')
			bL = float('inf')
			for a in ca:
				aL = min(aL,lA[pA.index(a)])
				bL = min(bL,lB[pB.index(a)])

			mrca = []
			for a in ca:
				if lA[pA.index(a)]==aL\
				and lB[pB.index(a)]==bL:
					mrca.append(a)

			aL = lA[pA.index(mrca[0])]
			bL = lB[pB.index(mrca[0])]		

			

			s = 'SELECT name,id FROM people WHERE id=?'
			cText = 'Most recent common ancestors:'+newLine
			for c in mrca:
				for row in run_query(s,(c,)):
					cText+=name_html(row,newLine)+newLine
			cText+=newLine
			cText+=nameList[0]+"'s "+parent_level(aL,'parent')\
				+newLine\
				+nameList[1]+"'s "\
				+parent_level(bL,'parent')			

			related = relationship(aL,bL,nameList)

	out = related + newLine + newLine+cText

	if related[-11:]!='not related':
	 	image = "<img src = jointAncestorGraph.py?id=%s&id2=%s&mL=%s>"\
        	        %(IDA,IDB,max(aL,bL))
	else:
		image = ''


        out +=newLine + image+newLine

	marriageSelect = 'SELECT Related FROM marriages WHERE ida=? AND idb=?'
	marriageID = (min(int(IDA),int(IDB)),max(int(IDA),int(IDB)))


	for row in run_query(marriageSelect,marriageID):
		u = "UPDATE marriages SET Related = ?"\
			+" WHERE ida = ? AND idb=?;"
		v = (related,)+marriageID
		run_query(u,v)
		commit_changes()

		break



        return [out,mrca,related]


def relationship(level1, level2,names):

	if level1==0 and level2==0:
		return "%s is %s" %(names[0],names[1])
	if level1==0:
		return "%s is %s's %s" \
			%(names[0],names[1],parent_level(level2,'parent'))

	if level2==0:
		return "%s is %s's %s" \
			%(names[1],names[0],parent_level(level1,'parent'))

	if level1>=level2:
		remove = level1-level2
		cousinNum = level2-1
	else:
		remove = level2-level1
		cousinNum = level1-1

	if cousinNum==0:
		uaLevel =  parent_level(remove,'uncle or aunt')
		if level1<= level2:
			return "%s is %s's %s" % (names[0],names[1],uaLevel)

		if level2<level1:
			return "%s is %s's %s" % (names[1],names[0],uaLevel)

	c=ordinal_numbers(cousinNum)
	if remove == 1:
		rem = 'once'
	elif remove ==2:
		rem = 'twice'
	else:
		rem = str(remove) + ' times'		

	r = "%s and %s are %s cousins" % (names[0],names[1],c)
	if remove !=0:
		r += ' %s removed' % rem

	return r

def parent_level(level,type):
	if level == 0:
		if type=='parent':
			return 'self'
		else:		
			return 'sibling'
	out = type
	if level ==1:
		return out
	if type =='parent':
		out = 'grand ' + out
	else:
		out = 'great '+out
	if level ==2:
		return out
	for i in range(2,level):
		out = 'great '+out
	return out

def rulers_of(aTerritory,newLine):
	p = aTerritory
	places = []
	
	for ap in [p,swap_gender(p)]:
		while predeccessorStates.has_key(ap):
			ap = predeccessorStates[ap][0]
			places.append(ap)
	
	out = "Previously:"+newLine
	for p in places:
		out+=terr_html(p,newLine,0,0) +newLine

	places=[]
	p = aTerritory
	for ap in [p,swap_gender(p)]:
		while successorStates.has_key(ap):
			ap = successorStates[ap]
			places.append(ap)
	out+='Subsequently:'+newLine
	for p in places:
		out+=terr_html(p,newLine,0,0) +newLine


	out += find_rulers(aTerritory,newLine)[0]


	image = "<img src = rulersGraph.py?terr=%s>" \
		%(re.sub(' ','%20',aTerritory))
	out+=image
	return out

def find_rulers(aTerritory,newLine):
	p = aTerritory

	out = ''
	fullTerr = []
	
	p = make_singular(p)
	s = "SELECT DISTINCT short"\
		+" FROM styleDecomp"\
		+" WHERE short LIKE ?"\
		+" OR short LIKE ?"\
		+" OR short LIKE?;"
	t = ('%'+p+'%','%'+swap_gender(p)+'%','%'+make_plural(p)+'%')


	for row in run_query(s,t):
		fullTerr.append(row[0])



	tq = "SELECT styleDecomp.style,name,people.ID,"\
		+" startyear,stopyear,short"\
		+" FROM styleDecomp INNER JOIN styles"\
		+" ON styleDecomp.style = styles.style"\
		+" INNER JOIN people"\
		+" ON people.ID = styles.ID"\
		+" WHERE short IN ("

	for i in range(len(fullTerr)):
		tq+='?,'
	tq = tq[:-1]	
	tq+=" )ORDER BY startyear,stopyear;"
	s = ''

	t = tuple(fullTerr)
	rulers = []
	for row in run_query(tq,t):
		if row[0]!=s:
			out +=newLine+row[0]+newLine
			s = row[0]
		if row[3]==0:
			fy = '?'
		else:
			fy = str(row[3])
		if row[4]==0:
			ft = '?'
		elif row[4]==1000000:
			ft = 'present'
		else:
			ft = str(row[4])
		out += "%s %s from %s to %s%s"\
		%(name_html(row[1:],newLine),row[5],\
		fy,ft,newLine)
		if row[2] not in rulers:
#			rulers.append(row[2])
			rulers = [row[2]]+rulers

	return [out,rulers]

def find_person(ID):
        s = "SELECT name || ','||ID, name, ID FROM people WHERE ID=?"
        t = (ID,)

        for row in run_query(s,t):
                Self = row[0]
		selfName = row[1]
		selfID = row[2]
                return [Self, selfID,selfName]
def isKing(ID):
	ID = int(ID.split(',')[-1])
	s=  "SELECT style FROM styles WHERE ID=?"
	t = (ID,)	

	k = 0
	spellingsOfKing = ['King','Queen','king','queen']
	for row in run_query(s,t):
		for s in spellingsOfKing:
			if re.search(s+'([^a-zA-Z]|$)',row[0]) != None:
				k = 1

	return k

def find_parents(ID):
        s = "SELECT name, parentID"\
                +" FROM parents LEFT JOIN people"\
                +" ON people.ID = parentID"\
                +" WHERE parents.ID = ?;"
        t = (ID,)

        parents = []
        parentIDs =[]
	parentNames=[]

        for row in run_query(s,t):
                if row[0]!=None:
                        p = row[0] + ',' + str(row[1])
                        pID = row[1]
			pN = row[0]
                else:
                        p = row[1] + ',p' + str(ID)
                        pID = 0
			pN = row[1]
                parents.append(p)
                parentIDs.append(pID)
		parentNames.append(pN)

	if len(parents)>1 and parents[1]==parents[0]:
		parents[1] = parents[1] + ' 2'

        return [parents,parentIDs,parentNames]


def find_ancestors(ID,ancestors,levels,names,level):
	ps, pIDs, pNs = find_parents(ID)
	
	for i in range(len(ps)):
		pID = pIDs[i]
		pN = pNs[i]
		ancestors.append(pID)
		levels.append(level+1)
		names.append(pN)
		find_ancestors(pID,ancestors,levels,names,level+1)

	return [ancestors,levels,names]


def is_married(IDa, IDb):
	s = "SELECT idb FROM marriages WHERE ida =?;"
	t = (min(IDa,IDb),)
	
	for row in run_query(s,t):
		if int(row[0])==max(IDa,IDb):
			return 1
	return 0	

def find_spouses(ID):
        t = (ID,)


        spouses = []
        spousesID=[]
	spousesNames=[]
	spouseDates = []

	s = "SELECT ida,a.name,idb,b.name,marriage,end,"\
	+" a.bornYear,a.diedYear,b.bornYear,b.diedYear,marriageYear,"\
	+" a.diedMonth,a.diedDay,b.diedMonth,b.diedDay"\
	+" FROM marriages"\
	+" LEFT JOIN people AS a"\
	+" ON ida = a.ID"\
	+" LEFT JOIN people AS b"\
	+" ON idb = b.ID"\
	+" WHERE ida = ? OR idb = ?"\
	+" ORDER BY marriageYear;"\

	t = (ID,ID)
	for row in run_query(s,t):
		sID = ''
		if row[0]!=int(ID): #spouse is ida
			if row[1]!=None:
				s = row[1]+","+str(row[0])
				sID = row[0]
				sN = row[1]
			elif row[0]!='':
				s = row[0]+",s"+str(ID)
				sID = 0
				sN = row[0]
			myDates = [row[1],row[0],row[4],row[5],row[6],\
			row[7],row[11],row[12]]
		else: #spouse is idb
			if row[3]!=None:
				s = row[3]+","+str(row[2])
				sID = row[2]
				sN = row[3]
			elif row[2]!='':
				s = row[2]+",s"+str(ID)
				sID = 0
				sN=  row[2]	
			myDates = [row[3],row[2],row[4],row[5],row[8],row[9],\
				row[13],row[14]]
		if sID!='':
			spouses.append(s)
			spousesID.append(sID)
			spousesNames.append(sN)
			spouseDates.append(myDates)

        return [spouses,spousesID,spousesNames,spouseDates]
	

def find_children(ID):
        s = "SELECT p1.name, p1.ID,p3.parentID,p4.name,p1.bornYear"\
                +" FROM people p1"\
                +" INNER JOIN parents p2"\
                +" ON p1.ID = p2.ID"\
                +" INNER JOIN parents p3"\
                +" ON p1.ID = p3.ID"\
                +" LEFT JOIN people"\
                +" p4 ON p3.parentID = p4.ID"\
                +" WHERE p2.parentID = ?"\
                +" AND p3.parentID<>?"\
                +" ORDER BY p1.bornYear;"

        t = (ID,ID)

	childrenBorn=[]
	nodes=[]
	IDs=[]	
	names=[]

        for row in run_query(s,t):
                c = row[0] + ',' + str(row[1])
                cID = row[1]
		cName = row[0]
		born = row[4]
                childrenBorn.append(born)
		if row[3]!=None:
                        op = row[3] + ',' + str(row[2])
                        opID = row[2]
			opN = row[3]
                else:
                        op = row[2] + ',s' + str(ID)
                        opID = 0
			opN = row[2]

		nodes.append([c,op])
		IDs.append([cID,opID])
		names.append([cName,opN])

        return [nodes,IDs,names,childrenBorn]

def person_info(personID,newLine):
	t = (personID,)

	if newLine=='<br>':
		startP = '<p>'
		endP = '</p>'
	else:
		startP = ''
		endP = newLine

	mainDiv = ''	
	#Id, Name, Dates, Style, Style-Dates
	s = "SELECT id,name,fullname,url,picture,Born,Died,"\
		+" bornYear,diedYear,diedMonth,diedDay"\
		+" FROM people WHERE ID = ?"
	rows = 0
	for row in run_query(s,t):
		rows = 1
		mainDiv += startP
		preURL = name_html(['previous',row[0]-1],newLine)
		postURL =  name_html(['next',row[0]+1],newLine)
		mainDiv += 'ID: %s(%s %s)%s' %(row[0],preURL,postURL ,newLine)
		mainDiv += print_tagged_name('Name',[row[1], row[0]]\
			,newLine)
		mainDiv+='Full Name: '+row[2]+newLine
		mainDiv +=endP
		name = row[1]
		fullname = row[2]
		url = row[3]
		picture = row[4]
		born = row[5]
		died = row[6]
		bornYear = row[7]
		diedYear = row[8]
		diedMonth = row[9]
		diedDay = row[10]

		mainDiv += startP
		mainDiv += '%sBorn:%s%s '% (newLine,born,newLine)



		if died!='present':
			mainDiv +='Died: %s' % died
	
			if diedYear != 0 and bornYear !=0:

				u = "SELECT age FROM"+ageTable\
					+" WHERE id = ?"
				
				for row in run_query(u,t):
					mainDiv += ", aged %s" %(row[0])
		else:
			u = "SELECT age FROM" + ageNowTable\
				+" WHERE id = ?"

			for row in run_query(u,t):
				mainDiv +='Still Alive, aged %s' %(row[0])

			

		mainDiv +=endP

	if rows==0:
		return ''

	s = "SELECT * FROM styles WHERE ID = ?"
	for row in run_query(s,t):
		mainDiv += startP
		mainDiv +='%sStyle: %s%s'%(newLine,row[1],newLine)

		mainDiv += 'Territories:%s' % newLine

		w = "SELECT short FROM styleDecomp"\
		+" WHERE style = ?"

		for r in run_query(w,(row[1],)):
			[p,n]=find_adjacent(r[0],row[3],row[5],personID)
			if len(p)>0:
				mainDiv +='%s|'%(name_html(p,newLine))

			mainDiv+=terr_html(r[0],newLine,row[3],row[5])

			if len(n)>0:
				mainDiv+='|%s'%(name_html(n,newLine))
			mainDiv+=newLine

		mainDiv +=  'From: '+row[2] + newLine
                mainDiv +=  'To: '+row[4]	

		mainDiv += endP




	mainDiv += startP
	mainDiv += endP

	#find parents

	[parents,parentIDs,parentNames] = find_parents(personID)
	mainDiv += startP
	for i in range(len(parents)):
		r = [parentNames[i],parentIDs[i]]
		mainDiv += print_tagged_name('Parent',r,newLine)
	mainDiv += endP

	#find spouses

	[spouses,spousesID,spousesNames,spouseDates] = find_spouses(personID)

	mainDiv += startP

	for i in range(len(spouses)):
		r = [spousesNames[i],spousesID[i]]
		d = spouseDates[i][2:8]

		mainDiv += print_tagged_name('Spouse',r,newLine)
		if d[0]!='.':
			mainDiv+='married: '+d[0]+newLine
		else:
			mainDiv+='marriage dates not yet recorded'\
			+ newLine
		if d[1]!='.':
			mainDiv+='marriage ended: '+d[1]+newLine
		elif d[0]!='.':
			dPrint = 1
			if d[3]<diedYear:
				if d[3]==0:
					y = '?'
				else:
					y = str(d[3])
				ot = 'their'
			elif d[3]==diedYear and diedYear!=1000000:
				y = str(d[3])
				
				if d[4]<diedMonth:
					ot = 'their'
				elif d[4]>diedMonth:
					ot = 'own'
				else:
					if d[5]<diedDay:
						ot = 'their'
					elif d[5]>diedDay:
						ot = 'own'
					else:
						dPrint = 0
						mainDiv+='until both of their deaths in %s%s'\
							%(y,newLine)

			else:
				if diedYear==1000000:
					dPrint = 0
					mainDiv+='still married%s'%(newLine)
				else:
					if diedYear==0:
						y = '?'
					else:
						y = str(diedYear)
					ot='own'
			if dPrint==1:
				mainDiv+='until %s death in %s%s' %(ot,y,newLine)
		
		mainDiv += \
			relationship_html(personID,r[1],newLine)\
			+newLine


		marriageStyles = ''
		mStart = int(findYear.find_year(d[0]))
		mStop = int(findYear.find_year(d[1]))

		if mStart==0:
			continue


		ss = "SELECT styles.style, short,startyear,"\
		+" stopyear"\
		+" FROM styles INNER JOIN styledecomp"\
		+" ON styles.style = styledecomp.style"\
		+" WHERE  id = ?;"
		st = (spousesID[i],)

		for sr in run_query(ss,st):
			if mStart>sr[3]:
				continue
			if mStop<sr[2] and mStop != 0:
				continue


			if mStart>sr[2]:
				fromy = mStart
			else:
				fromy = sr[2]

			if mStop<sr[3] and mStop !=0:
				to = str(mStop)
			elif diedYear<sr[3]:
				if diedYear==0:
					to = '?'
				elif diedYear==100000:
					to = 'present'
				else:
					to = str(diedYear)
			else:
				to = str(sr[3])
			marriageStyles+='%d to %s: %s%s' \
			%(fromy, to,swap_gender(sr[1]),newLine)
		if len(marriageStyles)>0:
			mainDiv+="Through this marriage:"+newLine
			mainDiv+=marriageStyles+newLine

	mainDiv  = mainDiv + newLine+endP

	#find children
	[nodes,IDs,names,childrenBorn] = \
		find_children(personID)

	top = ''
	for i in range(len(nodes)):
		cr = [names[i][0],IDs[i][0]]
		thisChild = print_tagged_name('Child',cr,newLine)

		opr=[names[i][1],IDs[i][1]]
		top = names[i][1]
		if i==0 or  top != names[i-1][1]:
			mainDiv +=endP
			mainDiv += startP
			mainDiv += print_tagged_name\
                        ('With',opr, newLine)


		#age when child born
		cb = childrenBorn[i]
		if  cb!=0 and  bornYear != 0:

			cs = "SELECT age,pID,cID FROM "+ageChildTable\
				+"WHERE pID=? AND cID =?"
			ct = (personID,IDs[i][0])

			for row in run_query(cs,ct):
				thisChild = thisChild[:-4]+\
				" at the age of "+str(row[0]) +newLine
		mainDiv += thisChild
	
	mainDiv += endP


	if newLine == '<br>':
		output = '<div id = "main" style = " float:left;width:75%">';
		output += mainDiv +  "</div>"

		output += "<div id = 'image' "\
			+"style = 'float:left; margin-left:20px'>"

		imageDiv = ''
		if picture!='.':
        		imageDiv += "<a href=" + url+">"\
                	+"<img src=" + picture +" alt = 'wiki link'"\
                	+" title = 'wiki link'></a>"\
                	+ newLine

     		elif url!='.' and url!='. ':
          		imageDiv += "<a href=" + url +">"\
         		+name + " (wiki link)</a>"+newLine

		output += imageDiv + "</div>"


		url = 'http://www.chiark.greenend.org.uk/ucgi/~naath/'\
			+'smallGraph.py'

		form = ''
		form += "<form id ='controlForm'"\
		+" action ="+ url +" method = 'get'>"

		form +=\
			"<input type = 'hidden' name = 'ID' value = "\
			+personID + "><br>"

                form +=\
		"Generations of Parents: "\
		+"<input type = 'text' name = 'pl' value='1'>"
		form += newLine
		form += \
		"Generations of Children: "\
		+" <input type = 'text' name = 'cl' value = '1'>"
		form += newLine
                form += \
                "Show siblings: <select name = 's'>"+\
                "<option value = '0'>No</option>"+\
                "<option value = '1'>Yes</option>"+\
		"</select>"
		form += newLine
                form += \
                "Show spouse's other spouses: <select name = 'os'>"+\
                "<option value = '0'>No</option>"+\
                "<option value = '1'>Yes</option>"+\
                "</select>"
		form += newLine
                form += \
                "Show parents' other spouses: <select name = 'pos'>"+\
                "<option value = '0'>No</option>"+\
                "<option value = '1'>Yes</option>"+\
                "</select>"		
		form += newLine
                form += \
		"Fount size: "+\
                "<input type = 'text' name = 'fs' value='8'>"
                form += newLine
        	form += "</form>"

		graph =  "smallGraph.py?ID="+str(personID)+"&fs=8"

		graph = "<img src ="+ graph + '>'

		output += "<div id = 'graph' style = 'clear:both'>"
		output += "<p id = 'agraph'>"+graph+"</p>"
		output += "Draw this graph with more relatives:"
		output += newLine + form
		
		output += "<button onclick='myFunction()'>"+\
			"Go</button>"

		output += "</div>"

		output +=\
		'<script>'+\
		'function myFunction()'+\
		'{'+\
		'var x = document.getElementById("controlForm");'+\
		'var txt = "<img src = " + x.action + "?";'+\
		'for (var i=0;i<x.length;i++)'+\
		'{'+\
		'var n=x.elements[i].name;'+\
		'var v=x.elements[i].value;'+\
		'txt = txt + "&"+n+"="+v;'+\
		'}'+\
		'txt = txt + ">";'+\
		'document.getElementById("agraph").innerHTML=txt;'+\
		'}'+\
		'</script>'

	else:
		output = mainDiv

	return output

def connect():
	global conn
	conn = sqlite3.connect\
		('/home/naath/familyTreeProject/familyTree/tree.db')
	return conn

def make_cursor():
	return conn.cursor()

def commit_changes():
	conn.commit()
	
def close(conn):
	conn.close

