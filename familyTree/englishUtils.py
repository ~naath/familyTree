#!/usr/bin/python
import re
import askQuestion as aQ

def reverse_dictionary(dict):
        rev_dict={}

        for item in dict.items():
                key = item[0]
                value = item[1]
                if value in rev_dict.keys():
       	                a = rev_dict[value]
               	        a.append(key)
                       	rev_dict[value] = a
               	else:
                       	rev_dict[value]=[key]

        return rev_dict

def swap_gender(style):
	for i in range(len(maleStyles)):
		m = maleStyles[i]+' '
		f = femaleStyles[i]+' '
		p = pluralStyles[i]+' '
		if re.search(f,style):
			return re.sub(f,m,style)
		elif re.search(m,style):
			return re.sub(m,f,style)
		elif re.search(p,style):
			return re.sub(p,m,style)

	return style

def make_plural(style):
	for i in range(len(maleStyles)):
                m = maleStyles[i]+' '
                f = femaleStyles[i]+' '
                p = pluralStyles[i]+' '
                if re.search(f,style):
                        return re.sub(f,p,style)
                elif re.search(m,style):
                      	return re.sub(m,p,style)
	return style
def make_singular(style):
	for i in range(len(maleStyles)):
		m = maleStyles[i]+' '
		f = femaleStyles[i]+' '
		p = pluralStyles[i] + ' '
		if re.search(p,style):
			return re.sub(p,m,style)
	return style
def make_male(style):
	for i in range(len(maleStyles)):
		m = maleStyles[i]+' '
                f = femaleStyles[i]+' ' 
                p = pluralStyles[i] + ' '
		if re.search(p,style):
			return re.sub(p,m,style)
		if re.search(f,style):
			return re.sub(f,m,style)
	return style
def is_number(s):
    try:
        float(s)
        return 1
    except ValueError:
        return 0

def print_people(n):
	if n!=1:
		return 'people'
	else:
		return 'person'

def print_children(n):
	if n>1:
		return 'children'
	else:	
		return 'child'

def month_numbers(monthN):
	names = ['unknown month', 'January','February',\
		'March','April','May','June','July',\
		'August','September','October',\
		'November','December']

	if monthN <=12:
		month = names[monthN]
	else:
		month = 'Incorrectly entered month ' + str(monthN)
	return month

def ordinal_numbers(number):
	number = int(number)
	out = '%d' % (number)
	if number % 10==1 and number/10 % 10 !=1:
		out +='st'
	elif number % 10==2 and number/10 % 10 !=1:
		out +='nd'
	elif number % 10==3 and number/10 % 10 !=1:
		out +='rd'
	else:
		out +='th'
	return out

def add_quotes(s):
	s = str(s)
	return "'"+s+"'"


def relationship(level1, level2,names):
	if len(names)==1:
		return "%s and this person are not related" %(names[0])

	if level1==0 and level2==0:
		if names[0]==names[1]:
			return "%s is %s" %(names[0],names[1])
		else:
			return "%s and %s are not related" %(names[0],names[1])
	if level1==0:
		return "%s is %s's %s" \
			%(names[0],names[1],parent_level(level2,'parent'))

	if level2==0:
		return "%s is %s's %s" \
			%(names[1],names[0],parent_level(level1,'parent'))

	if level1>=level2:
		remove = level1-level2
		cousinNum = level2-1
	else:
		remove = level2-level1
		cousinNum = level1-1

	if cousinNum==0:
		uaLevel =  parent_level(remove,'uncle or aunt')
		if level1<= level2:
			return "%s is %s's %s" % (names[0],names[1],uaLevel)

		if level2<level1:
			return "%s is %s's %s" % (names[1],names[0],uaLevel)

	c=ordinal_numbers(cousinNum)
	if remove == 1:
		rem = 'once'
	elif remove ==2:
		rem = 'twice'
	else:
		rem = str(remove) + ' times'		

	r = "%s and %s are %s cousins" % (names[0],names[1],c)
	if remove !=0:
		r += ' %s removed' % rem

	return r

def parent_level(level,type):
	selfList = ['parent','child']
	if level == 0:
		if type in selfList:
			return 'self'
		else:		
			return 'sibling'
	out = type
	if level ==1:
		return out
	if type in selfList:
		out = 'grand ' + out
	else:
		out = 'great '+out
	if level ==2:
		return out
	for i in range(2,level):
		out = 'great '+out
	return out

def isKing(ID):
	ID = int(ID.split(',')[-1])
	s= "SELECT style FROM styles WHERE ID=?"
	t = (ID,)	
	k = 0
	spellingsOfKing = ['King','Queen','king','queen']
	for row in aQ.run_query(s,t):
		for s in spellingsOfKing:
			if re.search(s+'([^a-zA-Z]|$)',row[0]) != None:
				k = 1
	return k

global maleStyles
maleStyles = ['Emperor','King','Duke','Archduke',\
                'Lord','Count','Prince','Baron','Landgrave',\
		'Treasurer','Bishop','bishop','Infante','Defender',\
		'Earl','Elector','Emperor','Margrave']
global femaleStyles
femaleStyles = ['Empress','Queen','Duchess','Archduchess',\
                'Lady','Countess','Princess','Baroness','Landgravine',\
		'Treasurer','Bishop','bishop','Infanta','Defender',\
		'Countess','Electress','Empress','Margravine']
global pluralStyles
pluralStyles = ['Emperors','Kings','Dukes','Archdukes',\
		'Lords','Counts','Princes','Barons','Landgraves',\
		'Treasurers','Bishops','bishops','Infantes','Defenders',\
		'Earls','Electors','Emperors','Margraves']

global successorStates
successorStates = {'King of the Anglo-Saxons':'King of the English',\
		'King of the English':'King of England',\
		'King of England':'King of Great Britain',\
		'King of Great Britain':\
		'King of the United Kingdom of Great Britain and Ireland',\
		'King of the United Kingdom of Great Britain and Ireland':\
	'King of the United Kingdom of Great Britain and Northern Ireland',\
		'King of the Franks':'King of France'\
		}

global predeccessorStates
predeccessorStates = reverse_dictionary(successorStates)
