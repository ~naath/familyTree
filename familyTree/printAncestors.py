#!/usr/bin/python

import askQuestion
import sys	


conn = askQuestion.connect()


if len(sys.argv) < 2:
	o = askQuestion.all_ancestors(1,'\n')
	print o[0]
elif len(sys.argv)==2:   
	o = askQuestion.all_ancestors(sys.argv[1],'\n')
	print o[0]
elif len(sys.argv)==3:
	o = askQuestion.common_ancestors(sys.argv[1],sys.argv[2],'\n')
	print o[0]
	
askQuestion.close()
