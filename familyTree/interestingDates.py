#!/usr/bin/python

import sqlite3
import findYear
from string import Template
import cgi
import re
import pickle
import pygraph.algorithms.accessibility as access
import englishUtils as eU
import printUtils as pU
import graphQuestions as gQ
import urllib2
import askQuestion as aQ
import csv


conn  = aQ.connect()
newLine = '\n'

filename = 'dates.csv'
with open(filename,'w') as csvfile:
                
	s = "SELECT died, count(*) AS n"\
                        +" FROM (SELECT died,diedYear"\
                        +" FROM people"\
                        +" WHERE diedMonth<>0 AND diedDay<>0)"\
                        +" GROUP BY died"\
                        +" HAVING n>1"\
                        +" ORDER BY diedYear;"

                
	t = "SELECT name,id"\
                        +" FROM people"\
                        +" WHERE died = ?;"


                
	mywriter = csv.writer(csvfile,delimiter = ';')
	for row in aQ.run_query (s,()):
		mywriter.writerow([row[0]])


