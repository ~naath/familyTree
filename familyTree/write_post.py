#!/usr/bin/python

import re
import askQuestion as aQ
import sys


def make_str(url,name,year,bd):
	u = "SELECT age FROM"+aQ.ageTable\
		+" WHERE id = ?"
	s = 'select name from parents inner'\
		+' join people on parents.parentID = people.ID'\
		+' where parents.ID=?;'
	o = bd+' on this day in ' +str(year)
	if bd=='Died':
		for r in aQ.run_query(u,(sys.argv[i],)):
			o+=' aged '+str(r[0])+' '
	else:
		o+=' to '
		p = 0
		for r in aQ.run_query(s,(sys.argv[i],)):
			o+=r[0] +' and '
			p = 1
		if p==1:
			o=o[:-5]+', '			
		else:
			o=o[:-3]
	o += name
	o+=' (<a href="http://www.chiark.greenend.org.uk/'
	o+='ucgi/~naath/person.py?ID='+str(sys.argv[i])
	o+='">my toy</a>,<a href="'+url+'">wikipedia</a>)'

	return o
aQ.connect()

for i in range(1,len(sys.argv)):
	s = "SELECT url,name,diedYear FROM people WHERE ID=?"
	t = (sys.argv[i],)
	u = "SELECT url,name,bornYear FROM people WHERE ID=?"

	if i==2:
		for r in aQ.run_query(u,t):
			url = r[0]
			name = r[1]
			year = r[2]


			print make_str(url,name,year,'Born')
	elif i==1:
		for r in aQ.run_query(s,t):
			url = r[0]
			name = r[1]
			year = r[2]
			print make_str(url,name,year,'Died')
			print ''



aQ.close()
