#!/usr/bin/python

import sqlite3
import findYear
from string import Template
import cgi
import re
import pickle
import pygraph.algorithms.accessibility as access
import englishUtils as eU
import printUtils as pU
import graphQuestions as gQ
import urllib2

global presentYear 
presentYear= 1000000
global ageTable
ageTable = "(SELECT diedYear-bornYear as age, name,ID,diedYear,bornYear"\
	+" FROM people"\
	+" WHERE bornYear<>0 AND diedYear<>0 AND (diedMonth>bornMonth"\
	+" OR (diedMonth==0)"\
	+" OR (diedMonth = bornMonth AND (diedDay>=bornDay OR diedDay =0)))"\
        +" UNION"\
       	+" SELECT diedYear-bornYear-1 as age,name,ID,diedYear,bornYear"\
        +" FROM people"\
        +" WHERE bornYear<>0 AND diedYear<>0 AND diedMonth<>0"\
	+" AND (diedMonth<bornMonth"\
        +" OR (diedMonth = bornMonth AND (diedDay<bornDay AND diedDay <>0)))"\
        +" )"

global ageNowTable
ageNowTable = "(SELECT strftime('%Y',date('now'))-bornYear as age,"\
	+"  name,ID,diedYear,bornYear"\
        +" FROM people"\
        +" WHERE bornYear<>0  AND (strftime('%m',date('now'))>bornMonth"\
        +" OR (strftime('%m',date('now')) = bornMonth AND"\
	+" (strftime('%d',date('now'))>=bornDay)))"\
        +" UNION"\
        +" SELECT strftime('%Y',date('now'))-bornYear-1 as age,"\
	+" name,ID,strftime('%Y',date('now')),bornYear"\
        +" FROM people"\
        +" WHERE bornYear<>0  AND (strftime('%m',date('now'))<bornMonth"\
        +" OR (strftime('%m',date('now')) = bornMonth"\
	+" AND (strftime('%d',date('now'))<bornDay)))"\
        +" )"

global ageChildTable
ageChildTable = "(SELECT c.bornYear - p.bornYear as age,"\
	+" c.name as cName,c.ID as cID,"\
	+" p.name as pname,p.ID as pID,"\
	+" c.bornYear as bornYear"\
	+" FROM"\
	+" parents INNER JOIN people c"\
	+" ON parents.ID = c.ID"\
	+" INNER JOIN people p"\
	+" ON parents.parentID = p.ID"\
	+" WHERE p.bornYear <>0 AND c.bornYear<>0"\
	+" AND (c.bornMonth>p.bornMonth"\
	+" OR (c.bornMonth = p.bornMonth AND c.bornDay>=p.bornDay))"\
	+" UNION"\
	+" SELECT c.bornYear - p.bornYear-1 as age,"\
	+" c.name as cName,c.ID as cID,"\
	+" p.name as pName,p.ID as pID,"\
	+" c.bornYear as bornYear"\
	+" FROM"\
	+" parents INNER JOIN people c"\
	+" ON parents.ID = c.ID"\
	+" INNER JOIN people p"\
	+" ON parents.parentID = p.ID"\
	+" WHERE p.bornYear <>0 AND c.bornYear<>0"\
	+" AND (c.bornMonth<p.bornMonth"\
	+" OR (c.bornMonth = p.bornMonth AND c.bornDay<p.bornDay))"\
	+" )"



def run_query(s,t):
	c = make_cursor()
	return c.execute(s,t)

def make_insert(table,fields):

        t = tuple(fields)
        values = '('
        for i in range(len(fields)):
                values = values+'?,'
        values = values[:-1]+')'

        s = 'INSERT INTO '+table+' VALUES'\
                +values
        run_query(s,t)

def number_people():
	s = 'SELECT max(ID) FROM people;'
	for r in run_query(s,()):
		return int(r[0])

def calendar(newLine):
	out = ''

	mLength = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	b = 'born'
	x = 'died'
	fcstart = '<FONT COLOR = "'
	fcmid = '">'
	fcend = '</FONT>'
	dcol = 'red'
	dcstart = fcstart+dcol+fcmid

	for m in range(12):

		n = count_born_on(m+1,0,b)
		o = count_born_on(m+1,0,x)
		p = count_born_in(m+1,b)
		q = count_born_in(m+1,x)

		script = 'birthday.py?date=%d-0' %(m+1)
		d = 0
		url = pU.link_Template.substitute\
                                (script = script,title = eU.month_numbers(m+1),\
                                text = eU.month_numbers(m+1))
		out += "%s %d %s%d%s %d %s%d%s%s" \
			%(url,n,dcstart,o,fcend,p,dcstart,q,fcend,newLine)


		out+="<table>"
		out+="<tr>"
		for d in range(mLength[m]):
			script = 'birthday.py?date=%d-%d' % (m+1,d+1)
			url = pU.link_Template.substitute\
	        		(script = script,title = str(d+1),\
                		text = str(d+1))

		        n = count_born_on(m+1,d+1,b)
			o = count_born_on(m+1,d+1,x)
	
			s = "<td>"
			e = "</td>"
	       		out+="%s%s%s %s%d%s %s%s%s%s%s " %(s,url,e,\
				s,n,e,s,dcstart,o,fcend,e)
			if (d+1)%7 ==0 or (d+1)==mLength[m]:
				out+="</tr>"
				out+="<tr>"
		out+="</table>"
		out+=newLine

	script = 'birthday.py?date=0-0'
        url = pU.link_Template.substitute\
        	(script = script,title = 'unknown',\
                text = 'unkown')

	n = count_born_on(0,0,b)
	o = count_born_on(0,0,x)
	out+="%s %d %s%d%s%s" %(url,n,dcstart,o,fcend,newLine)



	script = 'birthday.py?date=20-0'
        url = pU.link_Template.substitute\
                (script = script,title = 'on their birthday',\
                text = 'on their birthday')
	n = count_born_on(20,0,b)
	out +="%d people died %s%s" %(n,url,newLine)


	s = "select diedYear,count(*)"\
                +" FROM people"\
                +" WHERE diedYear==?"\
                +" GROUP BY diedYear;"
        for row in run_query(s,(presentYear,)):
                out += pU.print_age_death_count(row,newLine)

	for bd in ('born','died'):
		out+=newLine
		s = "SELECT "+bd+", count(*) AS n"\
			+" FROM (SELECT "+bd + "," + bd + "Year"\
			+" FROM people"\
			+" WHERE "+bd+"Month<>0 AND "+bd+"Day<>0)"\
			+" GROUP BY "+bd\
			+" HAVING n>1"\
			+" ORDER BY "+bd+"Year;"

		t = "SELECT name,id"\
			+" FROM people"\
			+" WHERE "+bd+" = ?;"
	

		out += "On the following days more than one person %s: %s" \
			%(bd,newLine)

		for row in run_query (s,()):
			out+=row[0]+":"+newLine
			for r in run_query(t,(str(row[0]),)):
				out+=pU.name_html(r,newLine)+newLine



	return out

def count_born_on(month,day,bd):
	if month==20:
		s = "SELECT count(*) FROM people"\
			+" WHERE bornMonth==diedMonth"\
			+" AND bornDay==diedDay"\
			+" AND bornMonth<>0"\
			+" AND diedDay<>0;"
		t = ()

	else:
		s = "SELECT count(*) FROM people"\
			+" WHERE "+bd+"Month==?"\
        	        +" AND "+bd+"Day==?"\
			+" AND "+bd+"Year<>?;"

		t = (month,day,presentYear)

	for row in run_query(s,t):
		return row[0]

def count_born_in(month,bd):

	s = "SELECT count(*) FROM people"\
		+" WHERE "+bd+"Month==?"\
		+" AND "+bd+"Year<>?;"
	t = (month,presentYear)

	for row in run_query(s,t):
		return row[0]

def born_on(date,newLine):
	month = int(date.split('-')[0])
	day = int(date.split('-')[1])

	fcstart = '<FONT COLOR = "'
        fcmid = '">'
        fcend = '</FONT>'
        dcol = 'red'
        dcstart = fcstart+dcol+fcmid


	out = ''


	if month==20:
		n = count_born_on(month,day,'')

		out+="%d %s died on their birthday: %s"\
			%(n, eU.print_people(n),newLine)

		s =  "SELECT name,id,bornYear,diedYear,bornMonth,bornDay"\
			+" FROM people"\
			+" WHERE bornMonth==diedMonth"\
                	+" AND bornDay==diedDay"\
                	+" AND bornMonth<>0"\
                	+" AND diedDay<>0"\
			+" ORDER BY diedYear-bornYear;"			
		t=()
		out+="Died on the day they were born:%s"%(newLine)
		out+=pU.table_header(['link','born and died'],newLine)
		header=0
		for row in run_query(s,t):
			if row[2]!=row[3] and header==0:
				header = 1
				out+=pU.table_foot(newLine)
				out+="%sDied on a later birthday:%s"\
					%(newLine,newLine)
				out+=pU.table_header(['link','born','died','month','day'],\
					newLine)
			link = pU.name_html(row,newLine)
			born = pU.print_year(row[2])
			died = pU.print_year(row[3])
			month = eU.month_numbers(row[4])
			day = str(row[5])
			if row[2]==row[3]:
				out+=pU.table_row([link,born],newLine)
			else:
				out+=pU.table_row([link,born,died,month,day],newLine)
		out+=pU.table_foot(newLine)
		return out

	if day!= 0:
		out += '%s %s %s' \
		%(eU.month_numbers(month),eU.ordinal_numbers(day),newLine)
	else:
		out +='%s %s'\
		%(eU.month_numbers(month),newLine)


	for bd in ['born','died']:
		s = "SELECT name,id,"+bd+"Year FROM people"\
			+" WHERE "+bd+"Month==?"\
			+" AND "+bd+"Day==?"\
			+" AND "+bd+"Year<>?"\
			+" ORDER BY "+bd+"Year;"

		t = (month,day,presentYear)
		n = count_born_on(month,day,bd)
		
		if bd=='died':
			out+=dcstart
		if day!=0:
			out+="%s %s %s on this day %s" \
			%(n,eU.print_people(n),bd, newLine)
		elif month!=0:
			out+="%s %s %s in this month on unknown day %s"\
			%(n,eU.print_people(n),bd,newLine)
		else:
			out+="%s %s %s in unknown month on unknown day %s"\
                	%(n,eU.print_people(n),bd,newLine)
		if bd=='died':
			out+=fcend
		out+=pU.table_header(['id','link',bd],newLine)
		for row in run_query(s,t):
			y = pU.print_year(row[2])

			link = pU.name_html(row,newLine)
			out+=pU.table_row([row[1],link,y],newLine)

		out+=pU.table_foot(newLine)
		out+=newLine

	if day==0 and month!=0:
		for bd in ['born','died']:
			s = "SELECT name,id,"+bd+"Year FROM people"\
                        +" WHERE "+bd+"Month==?"\
                        +" AND "+bd+"Year<>?"\
                        +" ORDER BY "+bd+"Year;"

	                t = (month,presentYear)
	                n = count_born_in(month,bd)	

	                if bd=='died':
        	                out+=dcstart
        	        out+="%s %s %s in this month %s" \
        	                %(n,eU.print_people(n),bd, newLine)
                	if bd=='died':
                        	out+=fcend

			out+=pU.table_header(['link',bd],newLine)
                	for row in run_query(s,t):	
				link = pU.name_html(row,newLine)
				y = pU.print_year(row[2])

				out+=pU.table_row([link,y],newLine)
			out+=pU.table_foot(newLine)
 	               	out+=newLine
					
	return out


def alive_on(day,month,year,newLine):

	bornBefore = "(bornMonth<? OR"\
			+" (bornMonth==? AND bornDay<=?))"

	diedAfter = "(diedMonth>? OR diedMonth==0"\
			+" OR (diedMonth==? AND (diedDay>=? OR diedDay==0)))"

	bornOrder = "bornyear,bornmonth,bornday"
	diedOrder = "diedyear,diedmonth,diedday"
	s = "SELECT name,ID,bornYear,diedYear FROM people"\
		+" WHERE"\
		+" bornYear<>0 AND diedYear<>0 AND("\
		+" (bornYear<? AND diedYear>?)"\
		+" OR"\
		+" (bornYear==? AND diedYear>? AND "+bornBefore+")"\
		+" OR"\
		+" (bornYear<? AND diedYear==? AND "+diedAfter+")"\
		+" OR"\
		+" (bornYear==? and diedYear==? AND "\
		+bornBefore+" AND "+diedAfter+")"\
		+")"\
		+"ORDER BY "+bornOrder+"," + diedOrder+";"

	yeart = (year,year)
	bbt=(month,month,day)
	dat = (month,month,day)
	t = yeart+yeart+bbt+yeart+dat+yeart+bbt+dat	
	out=pU.table_header(['link','born','died'],newLine)
	for row in run_query(s,t):
		link=pU.name_html(row,newLine)
		born = pU.print_year(row[2])
		died = pU.print_year(row[3])
		out+=pU.table_row([link,born,died],newLine)

	out+=pU.table_foot(newLine)

	return out

def list_people_parents():
	s = "SELECT name,id"\
		+" FROM people"\
		+" ORDER BY id;"

	output = []
	for row in run_query(s,()):

		ID = row[1]
		[parents, parentIDs,parentNames] = find_parents(ID)
		[spouses,spousesID,spousesNames,sD] = find_spouses(ID)
		
		[self,myID,myName] = find_person(ID)
		output.append([self,parents,spouses])
	return output


def list_people(newLine):
	s = "SELECT name,id,bornyear,fullname,diedYear,url"\
	+" FROM people"\
	+" ORDER BY bornyear,diedYear;"

	out = pU.table_header(['ID','link','wiki','name','born','died'],\
		newLine)
	year = 0
	out+=pU.table_row(['','',\
		'<b>Unknown Year</b>',0,0,''],newLine)
	for row in run_query(s,()):
		if row[2]!=0 and row[2]/100==0:
			out+=pU.table_row(['','','',\
			'<b>1st century</b>'\
			,1,100],newLine)
		if row[2]/100!=year/100:
			century = row[2]/100 + 1
			start = century * 100 -99
			end = century * 100
			out+=pU.table_row(['','','',\
                        '<b>'+eU.ordinal_numbers(century)+' century</b>',\
				start,end],newLine)

		
		ID = row[1]
		link = pU.name_html(row,newLine)
#		if row[5] != '.':
#			struct = fetch_page(row[5])
#			if struct.has_key('query'):
#		                pName = struct['query']['pages'].keys()[0]
#		                wikiName = struct['query']['pages'][pName]['title']
#			else:
#				wikiName = row[5]
#		else:
#			wikiName = ''
		wikiName = row[5]
		wikiLink = "<a href='" + wikiName + "'>"+wikiName+"</a>"

		a = re.search('#',wikiName)
		if a:
			wikiName = '#'
			wikiLink = wikiName
		if row[5]=='.':
			wikiName = 'None'
			wikiLink = wikiName


		name = row[3]
		born = pU.print_year(row[2])
		died = pU.print_year(row[4])

		out+=pU.table_row([ID,link,wikiLink,name,born,died],newLine)
		year = row[2]
	out+=pU.table_foot(newLine)
	return out

def list_page(newLine):
	s = "SELECT name,ID,url FROM people ORDER BY ID;"
	t = ()
	out = ''

	check=[]
	for row in run_query(s,t):
		if row[2]!='.':
			topLink = "%d	<a href='%s'>%s</a>" %(row[1],row[2],row[0])
			print row[1]
			myLink = pU.name_html(row,newLine)
			topLink = topLink+' '+myLink
			url = row[2]
			id = row[1]
			
			[fam,count,checkMe] = find_fam(id,url,newLine)
			if checkMe==0:
				out+=topLink+newLine+fam+newLine
			else:
				check.append([id,topLink,url,fam,count])

	outDone = out
	out = 'MATCHED'+newLine
	hold = 'CHECK COUNT'+newLine
	checkAgain=[]
	for p in check:
		id = p[0]
		topLink=p[1]
		url=p[2]
		fam=p[3]
		countF = p[4]
		print id
	
		links,countL = find_links(url)
		if links:
			if links[0]!=-1:
				this=topLink+newLine+fam+newLine

				this+='<ul>'
				for l in links:
					this+='<li>'+l+newLine+'\n'
				this+='</ul>'
				good=1
				for i in range(3):
					if countL[i]<countF[i]:
						good = 0;
				if good==1:
					out+=this
				else:
					hold+=\
			this+newLine+str(countF)+str(countL)+newLine

			else:
				checkAgain.append(p)
		else:
				checkAgain.append(p)
	outMatched = out
	outUnMatched = hold

	out=newLine + 'CHECK NO INFO'+newLine
	for p in checkAgain:
		id = p[0]
                topLink=p[1]
                url=p[2]
                fam=p[3]
		print id

		if re.search('#',url):
			continue
		out+=topLink+newLine+fam

	return [outDone,outMatched,outUnMatched,out]

def find_fam(id,link,newLine):
	out = ''

	[parents, parentIDs,parentNames] = find_parents(id)
	[spouses,spousesID,spousesNames,sD] = find_spouses(id)
	[nodes,IDs,names,childrenBorn] = \
	        find_children(id)
	
	relIDs = parentIDs+spousesID
	for ID in IDs:
		relIDs.append(ID[0])

	out+='<ul>'
	findUrl = 'SELECT url,name FROM people WHERE ID=?'
	findName = 'SELECT name,ID FROM people WHERE ID=?'
	anyCheck = 0;
	count = [0, 0, 0]
	for ID in relIDs:
		t = (ID,)
		if ID in parentIDs:
			type = 'parent'
			count[0]+=1
		elif ID in spousesID:
			type = 'spouse'
			count[1]+=1
		else:
			type = 'child'
			count[2]+=1

		for row in run_query(findUrl,t):
			url = row[0]

			if url!='.':
				url = row[0]
				title = row[1]
				out+='<li>'+type+" <a href="+url\
				+">"+title+"</a>"+newLine
			else:
				for row in run_query(findName,t):
#					out+='<li>'+pU.name_html(row,newLine)\
#					+'CHECK'+newLine
					name=row[0]
					id = str(row[1])
					out+='<li>'+type+' '+name + ' '+id+\
						' CHECK'+newLine
					anyCheck = 1
		
	out+='</ul>'
	return [out,count, anyCheck]

def fetch_page(url):
	import json

	title = url.split('/')[-1]
        url = 'http://en.wikipedia.org/w/api.php?'\
                +'format=json&action=query'\
                +'&titles='+title\
                +'&prop=revisions&rvprop=content&redirects'

        r = urllib2.urlopen(url)
        t = r.read()
        jd = json.JSONDecoder()
	struct = jd.decode(t)


	return struct

def find_infoBox(url):
	struct = fetch_page(url)
	try:
	        pages = struct['query']['pages'].keys()
	except:
		return

        startPatt = re.compile('{{',re.DOTALL)
        endPatt = re.compile('}}',re.DOTALL)
        infoboxPatt = re.compile('{{( )*[Ii]nfobox',re.DOTALL)

	isIBOX = 0
	for p in pages:
		try:
			page = struct['query']['pages'][p]['revisions'][0]['*']
		except:
			return

		title = struct['query']['pages'][p]['title']		
		iBox = re.search(infoboxPatt,page)
                starts = re.finditer(startPatt,page)
		ends = re.finditer(endPatt,page)

	 	if iBox==None:	
			continue

		myStart = iBox.start()
		isIBOX=1
		                        
		countMe = 0
		start = -1
                while start<myStart:
	                start = starts.next().start()
        	        end = -1
                while end<myStart:
                	end = ends.next().start()
                while 1==1:
                	if start<end:
				countMe+=1
				start = starts.next().start()
			elif end<start:
				countMe-=1
				myEnd = end
				end = ends.next().start()
			if countMe==0:
				break
		info = page[myStart+2:myEnd]

	if isIBOX==1:
		return info
	else:
		return 

def find_links(url):

	info = find_infoBox(url)

	if info:
		l,c = wikilink_box(info)
		return l,c
	else:
		return [-1],[]	

def wikilink_box(text):
	linkPatt = "([^=]*?)\[\[(.*?)\]\](.*?)"
	#linkPatt = "\|(.*?)\[\[(.*?)\]\](.*?)\n"	
	#linkPatt = "(.*?)\[\[(.*?)\]\]([^<]*?)[\\n|<br.*?>|,]"
	links = []

	commentPatt = "<!--.*?>"
	sizePatt = "<[/]*small>"
	text=re.sub(commentPatt,'',text)
	text = re.sub(sizePatt,'',text)

	pscPatt = "father|mother|parent|spouse|issue|child"
	typePatt = "(.*?)="
	lines = re.split('\\n',text)
	count = [0, 0,0]
	for line in lines:
		if re.search(pscPatt,line):
			t = re.search(typePatt,line)
			if t is None:
				continue
			type = t.group(1)
			if re.search('father|mother|parent',type):
				c=0
			elif re.search('spouse',type):
				c=1
			else:
				c=2
			people = re.split('<br.*?>',line)
			for p in people:
				count[c]+=1
				if re.search(linkPatt,p):
					l = re.search(linkPatt,p)
					url = wiki_to_url(l.group(2))
					t = l.group(2).split('|')[-1]

					myLink = '<a href="%s">%s</a>' \
					%(url,t)
					link = l.group(1)+myLink+l.group(3)
					add=type+link
				else:
					add = type+' '+p
				links.append(add)
	return links,count


def wiki_to_url(wiki):
	base = 'https://en.wikipedia.org/wiki/'
	u = wiki.split('|')[0]
	u = re.sub(' ','_',u)

	return base+u	

def check_true(newLine):
	s = "SELECT name,id,url,born,died FROM people WHERE id>? and id<? ORDER BY id;"
	t = (0,5000)

	out='<table border = "1" style = "width:100%">'
	out+='<tr>'
	out+='<th style="width:20%">Name</th>'
	out+='<th style="width:40%">Toy</th>'
	out+='<th style="width:40%">Wiki</th>'
	out+='</tr>'

	for row in run_query(s,t):
		if row[2]=='.':
			continue
		struct = fetch_page(row[2])
		try:
			p = struct['query']['pages'].keys()[0]
		except:
			continue

		title = struct['query']['pages'][p]['title']		
				
		topLink = "%d	<a href='%s'>%s</a>" %(row[1],row[2],title)
		
		myLink = pU.name_html(row,newLine)
		topLink = topLink+newLine+myLink
		url = row[2]
		id = row[1]
			
		[fam,count,checkMe] = find_fam(id,url,newLine)
		infobox = find_infoBox(url)


		if checkMe!=0:
			continue
		out+='<tr>'
		out+='<td>'+topLink+'</td>'
	
		if infobox is None:
			out+='</tr>'
			continue

		

		out+='<td>'+fam+newLine+'born:'+row[3]+newLine\
			+'died:'+row[4]+'</td>'


		wikiLink='\\[\\[([^\\|\\]]*)\\|*(.*?)\\]\\]'
		htmlLink=r'<a href="https://en.wikipedia.org/wiki/\1">\1</a>'

		htmlBox = re.sub(wikiLink,htmlLink,infobox)

		place = 0
		starts = re.finditer('{{',htmlBox)
		stops = re.finditer('}}',htmlBox)
		last = len(htmlBox)		

		try:
			start = starts.next().start()
			stop = stops.next().start()
		except:
			start = last
			stop = last

		lines=[]
		inList= 0

		for i in re.finditer('\|',htmlBox):
			if i.start()>start and start<stop:
				inList+=1
				try:
					start= starts.next().start()
				except:
					start = last
			elif i.start()>stop and stop<start:
				inList-=1
				try:
					stop = stops.next().start()
				except:
					stop = last

			if inList==0:
				lines.append(htmlBox[place:i.start()])
				place = i.start()

			
		of_interest = 'father|mother|parent|child|issue'\
			+'|spouse|birth_date|death_date'
	
		out+='<td>'

		parents = []
		children=[]
		spouse=[]
		dates=[]

		for l in lines:
			if re.search('father|mother|parent',l):
				parents.append(l)
			if re.search('child|issue',l):
				children.append(l)
			if re.search('spouse',l):
				spouse.append(l)
			if re.search('birth_date|death_date',l):
				dates.append(l)

		for p in parents:
			out+=p+newLine
		for s in spouse:
			out+=s+newLine
		for c in children:
			out+=c+newLine
		for d in dates:
			out+=d+newLine

		out+='</td></tr>'

	out+='</table>'
	return out




def search_name(name,newLine):
	s = "SELECT name, people.ID,fullname,BornYear,DiedYear"\
        +" FROM people"\
        +" WHERE fullname LIKE ? or name LIKE ?"\
	+" ORDER BY BornYear;"

	s2 = "SELECT family FROM families where ID=?"

	IDs=[]
	names=[]

	out = 'Names starting with %s:%s' % (name,newLine)
	t = (name + '%',name+'%')
	fullIDs=[]
	out+=pU.table_header(['link','name','born','died','family'],newLine)
	for row in run_query(s,t):
		b = pU.print_year(row[3])
		d = pU.print_year(row[4])

		id = row[1]
		fams=''
		for r in run_query(s2,(id,)):
			fams+=r[0]+' '

		out+=pU.table_row([pU.name_html(row,newLine),row[2],b,d,fams],\
			newLine)

		fullIDs.append(row[1])
		names.append(row[0])
		IDs.append(row[1])
	out+=pU.table_foot(newLine)

        t = ('%' + name + '%','%'+name+'%')
	out += '%sNames containing %s:%s' %(newLine,name,newLine)
	out+=pU.table_header(['link','name','born','died','family'],newLine)
        for row in run_query(s,t):
		if row[1] not in fullIDs:
			b = pU.print_year(row[3])
			d = pU.print_year(row[4])

			id = row[1]
			fams=''
			for r in run_query(s2,(id,)):
				fams+=r[0]+' '

			out+=pU.table_row([pU.name_html(row,newLine),\
				row[2],b,d,fams],newLine)
			names.append(row[0]+','+row[2])
			IDs.append(row[1])
	out+=pU.table_foot(newLine)

	s = '''SELECT name,people.ID,style,fullname,bornYear, diedYear,
	startYear,stopYear,family
	FROM people INNER JOIN styles
	ON styles.id = people.id
	LEFT JOIN families ON people.ID = families.ID
	WHERE style LIKE ?
	ORDER BY bornYear;'''
	
	out += '%sStyles containing %s:%s' %(newLine,name,newLine)
	out+=pU.table_header(['link','name','born',\
		'died','style','from','to','family'],newLine)
	t = (t[0],)
	for row in run_query(s,t):
		b = pU.print_year(row[4])
		d = pU.print_year(row[5])
		start = pU.print_year(row[6])
		stop = pU.print_year(row[7])
		out+=pU.table_row([pU.name_html(row,newLine),\
			row[3],b,d,row[2],start,stop,row[8]],newLine)

	out+=pU.table_foot(newLine)
        return [out,names,IDs]

def count_names(newLine):
	s="SELECT names.name, count(*),"\
	+" min(nullif(bornYear,0)),max(nullif(bornYear,0))"\
	+" FROM names INNER JOIN people on people.ID = names.id"\
	+" group by names.name"\


	t = "SELECT count(*) FROM people WHERE firstName = ?"
	
	out=pU.table_header(\
		['name','count','earliest born',\
		'latest born','range','count as first name'],\
		newLine)
	for row in run_query(s,()):
		for r in run_query(t,(row[0],)):
			c =r[0]
		if row[3] is not None and row[2] is not None:
			range = row[3]-row[2]
		else:
			range = None
		out+=pU.print_name_count(row,range,c,newLine)
	out+=pU.table_foot(newLine)
	
	return out

def people_with_name(name,newLine):
	s = "SELECT name, ID,fullname,bornYear,diedYear"\
	+" FROM people"\
	+" WHERE firstname = ?"\
	+" ORDER BY bornYear;"

	t = (name,)

	out='As a first name'+newLine
	out += pU.table_header(['ID','name','link','born','died'],newLine)
	for row in run_query(s,t):
		myName = pU.name_html(row,newLine)
		by = pU.print_year(row[3])
		dy = pU.print_year(row[4])
		
		out+=pU.table_row([row[1],row[2],myName,by,dy],newLine)

	out+=pU.table_foot(newLine)

	s = "SELECT people.name, people.ID,fullname,bornYear,diedYear"\
	+" FROM people"\
	+" LEFT JOIN names ON people.ID = names.ID"\
	+" WHERE names.name=? AND firstName<>?"\
	+" ORDER BY bornYear;"

	t = (name,name)
	out+=newLine+'As a middle name'+newLine
	out +=pU.table_header(['ID','name','link','born','died'],newLine)
	for row in run_query(s,t):
		myName = pU.name_html(row,newLine)
		by = pU.print_year(row[3])
		dy = pU.print_year(row[4])
		
		out+=pU.table_row([row[1],row[2],myName,by,dy],newLine)

	out+=pU.table_foot(newLine)
	


	return out


def count_children(newLine):

	s = "SELECT count(*),nc"\
	+" FROM ("\
	+" SELECT count(*) AS nc"\
	+" FROM parents INNER JOIN people ON parentID = people.ID"\
	+" GROUP BY parentID"\
	+" HAVING parentID <>'?'"\
	+" AND parentID <> '0')"\
	+" GROUP BY nc;"

	out = ''
	for row in run_query(s,()):
		out += pU.print_children_count(row,newLine)
	return out

def parents_with_children(nChildren,newLine):
	s = "SELECT name,parentID"\
	+ " FROM parents"\
	+ " INNER JOIN people"\
	+ " ON parentID = people.ID"\
	+ " GROUP BY parentID"\
	+ " HAVING count(*) = ?"\
	+ " AND parentID <> 0"\
	+ " ORDER BY bornYear;"


	u = "SELECT count(*)"\
	+" FROM  parents INNER JOIN people"\
	+" ON parents.ID = people.ID"\
	+" WHERE parentID = ?"\
	+" AND (diedyear-bornyear>? OR died='present');"

	out = "People who had %s %s:%s" \
		%(nChildren,eU.print_children(nChildren),newLine)

	out+=pU.table_header(['link','survived infancy','survived to adulthood'],newLine)
	for row in run_query(s,(nChildren,)):
		link = pU.name_html(row,newLine)
		for r in run_query(u,(row[1],1)):	
			infant = r[0]
		for r in run_query(u,(row[1],18)):
			adult = r[0]
		out +=pU.table_row([link,infant,adult],newLine)
	out+=pU.table_foot(newLine)
	return out


def count_age_at_child(newLine):

	s = "SELECT age,count(*)"\
		+" FROM "+ageChildTable\
		+" GROUP BY age ORDER BY age;"

	out = ''
	for row in run_query(s,()):
		out +=pU.print_age_child_count(row,newLine)

	return out

def people_had_child_at_age(age,newLine):


	s = "SELECT age,cname,cID,pname,pID,bornYear"\
		+" FROM "+ageChildTable\
		+" WHERE age = ?;"

	t = (int(age),)
	out = 'At age ' + str(age) + ' :'
	out+=pU.table_header(['parent','child','year'],newLine)
	for row in run_query(s,t):
		parent = pU.name_html([row[3],row[4]],newLine)
		child = pU.name_html([row[1],row[2]],newLine)
		year = pU.print_year(row[5])
		out+=pU.table_row([parent,child,year],newLine)
#		out += "%s%s had %s" % (newLine,parent,child)
	out+=pU.table_foot(newLine)
	return out

def count_age_at_death(newLine):


	s = "SELECT age,count(*)"\
		+" FROM"\
		+ ageTable\
		+" WHERE diedYear<>?"\
		+" GROUP BY age;"

	out=''
	for row in run_query(s,(presentYear,)):
		out += pU.print_age_death_count(row,newLine)

	s = "select diedYear,count(*)"\
		+" FROM people"\
		+" WHERE diedYear==?"\
		+" GROUP BY diedYear;"
	for row in run_query(s,(presentYear,)):
		out += pU.print_age_death_count(row,newLine)

	return out

def people_died_at_age(age,newLine):
	

	if age != str(presentYear):

		s = "SELECT age,name,ID,diedYear,bornYear"\
		+" FROM "\
		+ ageTable\
		+" WHERE age = ?"\
		+" ORDER BY diedYear;"


		t = (int(age),)
		out = 'Died at age ' +str(age)+newLine
		out+=pU.table_header(['link','in'],newLine)
		for row in run_query(s,t):
			link = pU.name_html([row[1],row[2]],newLine)
			year = pU.print_year(row[3])
			out+=pU.table_row([link,year],newLine)
	else:
		s = "SELECT diedYear, name,ID,bornYear,url"\
			+" FROM people"\
			+" WHERE diedYear = ?;"
		out = 'Still alive'+newLine
		out+=pU.table_header(['link','born in','wikipedia'],newLine)
		for row in run_query(s,(presentYear,)):
			link = pU.name_html([row[1],row[2]],newLine)
			born = pU.print_year(row[3])
			if row[4]=='.':
				wlink = ''
			else:
				wlink = '<a href="'+row[4]+'">wikipedia</a>'
			out+=pU.table_row([link,born,wlink],newLine)
	out+=pU.table_foot(newLine)
	return out


def all_ancestors(personID,newLine):


	s = "SELECT name,id FROM people WHERE id=?"
	t = (personID,)
	out=pU.print_tagged_query("Ancestors of ",s,t,newLine)

	names = ['me']

	allAncestors,levelDict = gQ.ancestors(int(personID))

	for level in levelDict.keys():

		out += eU.parent_level(level,'parent')+':'+newLine

		for a in levelDict[level]:
			if eU.is_number(a):
				for r in run_query(s,(a,)):
					out+=pU.name_html(r,newLine)+newLine
			else:
				out+=a+newLine

	image = "<img src = ancestorGraph.py?id=%s>" %personID
	out +=newLine + image+newLine

	return [out, allAncestors,levelDict]
def all_descendants(personID,newLine):
	s = "SELECT name,id,bornYear,diedYear FROM people WHERE id=?"
	t = (personID,)
	out = pU.print_tagged_query("Descendants of ",s,t,newLine)
	names = ['me']
	allDescendants,levelDict = gQ.descendants(int(personID))
	for level in levelDict.keys():
		out+=eU.parent_level(level,'child')+':'+newLine


		out+=pU.table_header(['Name','Born','Died'],newLine)
		for a in levelDict[level]:
			if eU.is_number(a):
				for r in run_query(s,(a,)):
					n=pU.name_html(r,newLine)
					b = r[2]
					d = r[3]
			else:
				n=a
				b=0
				d=0
			if b==0:
				b = '?'
			if d==0:
				d = '?'
			out+=pU.table_row([n,b,d],newLine)

		
		out+=pU.table_foot(newLine)

	return [out, allDescendants,levelDict]
def check_relationship(a,b):
	s = 'SELECT related FROM marriages'\
		+' WHERE ida =? AND idb=? AND related<>0'
	t = (min(a,b),max(a,b))
	for r in run_query(s,t):
		return r[0]

def update_relationship(a,b,r,commit):
	s = 'SELECT related FROM marriages'\
		+' WHERE ida =? AND idb=?'
	t = (min(a,b),max(a,b))

	for row in run_query(s,t):
		u = "UPDATE marriages SET Related = ?"\
		+" WHERE ida = ? AND idb=?;"
		v = (r,)+t
		try:
			run_query(u,v)
			if commit==1:
				commit_changes()
			return 
		except:
			return 


def find_relationship(IDA,IDB,commit):
	nameList=[]
	s  = "SELECT name,id FROM people WHERE id ==?"
	for id in (IDA,IDB):
		t = (id,)
		for row in run_query(s,t):
			nameList.append(pU.name_html(row,'<br>'))
				
	
	mrca,orca,aL,bL = gQ.relationship(IDA,IDB)
	related = eU.relationship(aL,bL,nameList)
	update_relationship(IDA,IDB,related,commit)
	return related 

def relation_text(IDA,IDB,newLine):
	
	IDA = int(IDA)
	IDB = int(IDB)
	related = check_relationship(IDA,IDB)
	
	if related is None:
		related = find_relationship(IDA,IDB,1)
		
	
	return related

def common_ancestors(IDA,IDB,newLine):
	related = relation_text(IDA,IDB,newLine)
	out = related+newLine

	if related[-11:]!='not related':	
		mrca,orca,aL,bL = gQ.relationship(int(IDA),int(IDB))		
		s = 'SELECT name,id FROM people WHERE id=?'
		cText = 'Most recent common ancestors:'+newLine
		for c in mrca:
			for row in run_query(s,(c,)):
				cText+=pU.name_html(row,newLine)+newLine
		cText+=newLine
		findName = re.compile('(<a.*?</a>) and (<a.*?</a>).*')
		findNameP = re.compile('(<a.*?</a>) is (<a.*?</a>).*')
		found = findName.search(related)
		if found is None:
			found = findNameP.search(related)
		nameA = found.group(1)
		nameB = found.group(2)
		cText+=nameA+"'s "+eU.parent_level(aL,'parent')\
			+newLine\
			+nameB+"'s "\
			+eU.parent_level(bL,'parent')


		out += newLine+cText
	
	 	image = "<img src = jointAncestorGraph.py?id=%s&id2=%s&mL=%s>"\
        	        %(IDA,IDB,max(aL,bL))

       		out +=newLine + image
	out+=newLine

        return out

def list_territories(newLine):
	s = "SELECT DISTINCT short"\
	+" FROM styleDecomp"\
	+" ORDER BY short;"

	out = ''
	terrs = []
	for row in run_query(s,()):
		match = 0
		
		for i in range(len(eU.maleStyles)):
			m = eU.maleStyles[i]+' '
			if re.search(m,row[0]):
				match = 1
				break
		if match==1:
			t = row[0]
		else:
			t = eU.swap_gender(row[0])			

		if t not in terrs:
			terrs.append(t)

	for i in range(len(terrs)):
		terrs[i] = eU.make_plural(terrs[i])
	
	for terr in terrs:
		out += pU.terr_html(terr,newLine,0,0) +newLine

	return out


def list_families(newLine):
	s = "SELECT family, count(*)"\
	+" FROM families GROUP BY family ORDER BY family;"
	out = pU.table_header(['family','members'],newLine)
	
	for row in run_query(s,()):
		link=pU.fam_html(row[0],newLine)
		count = row[1]
		out+=pU.table_row([link,count],newLine)


	out+=pU.table_foot(newLine)
	return out

def combine_states(aTerritory):
	p = aTerritory
	places = []

	predeccessorStates = eU.predeccessorStates
	successorStates = eU.successorStates
	
	ap = eU.make_male(aTerritory)
	while predeccessorStates.has_key(ap):
		ap = predeccessorStates[ap][0]
		places.append(ap)
	

	ap = eU.make_male(aTerritory)
	while successorStates.has_key(ap):
		ap = successorStates[ap]
		places.append(ap)

	return places

def people_in(fam,newLine):
	s = 'SELECT name,people.id,bornYear,diedYear FROM'\
	+' people INNER JOIN families'\
	+' ON people.id = families.id'\
	+' WHERE family = ? ORDER BY bornYear;'
	t = (fam,)

	out='Family: %s' %fam
	out+=newLine+newLine

	out += pU.table_header(['id','name','born','died'],newLine)
	ids=''
	for row in run_query(s,t):
		name=pU.name_html(row,newLine)
		
		b = row[2]
		d = row[3]
		if b==0:
			b='?'
		if d==0:
			d = '?'
		if d==presentYear:
			d='present'
		out+=pU.table_row([row[1],name,b,d],newLine)
		ids+='%d,' % row[1]
	ids=ids[:-1]
	out+=pU.table_foot(newLine)

	image = "<img src = rulersGraph.py?names=%s>"\
		% ids
	out+=image
	return out


def rulers_of(aTerritory,newLine):
	
	out = pU.terr_html(eU.make_male(aTerritory),newLine,0,0)+newLine
	
	otherStates = combine_states(aTerritory)
	if len(otherStates)>0:
		out+="Otherwise:"+newLine
	for s in otherStates:
		out+=pU.terr_html(s,newLine,0,0)+newLine

	out += find_rulers(aTerritory,newLine)[0]


	image = "<img src = rulersGraph.py?terr=%s>" \
		%(re.sub(' ','%20',aTerritory))
	out+=image
	return out

def find_rulers(aTerritory,newLine):
	places = [eU.make_singular(aTerritory)]
	places+=combine_states(aTerritory)

	out = ''
	fullTerr = []

	for p in places:
		s = "SELECT DISTINCT short"\
			+" FROM styleDecomp"\
			+" WHERE short LIKE ?"\
			+" OR short LIKE ?"\
			+" OR short LIKE?;"
		t = ('%'+p+'%','%'+eU.swap_gender(p)+'%',\
		'%'+eU.make_plural(p)+'%')


		for row in run_query(s,t):
			fullTerr.append(row[0])


	if len(fullTerr)==0:
		return out,[]
		
	tq = "SELECT styleDecomp.style,name,people.ID,"\
		+" startyear,stopyear,short"\
		+" FROM styleDecomp INNER JOIN styles"\
		+" ON styleDecomp.style = styles.style"\
		+" INNER JOIN people"\
		+" ON people.ID = styles.ID"\
		+" WHERE short IN ("

	for i in range(len(fullTerr)):
		tq+='?,'
	tq = tq[:-1]	
	tq+=" )ORDER BY startyear,stopyear;"
	s = ''

	t = tuple(fullTerr)
	rulers = []
	out+=pU.table_header(['link','style','from', 'to','house'],newLine)
	for row in run_query(tq,t):
		#if row[0]!=s:
		#	out +=newLine+row[0]+newLine
		#	s = row[0]
		fy = pU.print_year(row[3])
		ft = pU.print_year(row[4])
		

		id = row[2];
		u = "SELECT family FROM families WHERE id = ?"
		v = (id,)
		house = ''
		for r in run_query(u,v):
			house+=pU.fam_html(r[0],newLine)

		out+=pU.table_row([pU.name_html(row[1:],newLine),\
			row[0],fy,ft,house],newLine)
		
		if row[2] not in rulers:
#			rulers.append(row[2])
			rulers = [row[2]]+rulers
	out+=pU.table_foot(newLine)
	return [out,rulers]

def find_adjacent(terr,start,stop,id):
	s = "SELECT name,people.id,stopyear"\
	+" FROM people INNER JOIN styles"\
	+" ON people.id = styles.id"\
	+" INNER JOIN styleDecomp"\
	+" ON styles.style = styleDecomp.style"\
	+" WHERE short = ? AND stopyear<=? AND stopyear <>0 AND people.id<>?"\
	+" ORDER BY stopyear DESC, startyear DESC;"


	t = (terr,start,id)
	myPrevious=[]
	y=0
	for row in run_query(s,t):
		myPrevious = row[0:2]
		y = row[2]
		break

	t = (eU.swap_gender(terr),start,id)
	for row in run_query(s,t):
		if row[2]>y:
			myPrevious = row[0:2]
		break

	u = "SELECT name,people.id,startyear"\
        +" FROM people INNER JOIN styles"\
  	+" ON people.id = styles.id"\
       	+" INNER JOIN styleDecomp"\
        +" ON styles.style = styleDecomp.style"\
        +" WHERE short = ? AND startyear>=? AND startyear <>0 AND people.id<>?"\
        +" ORDER BY startyear;"
		
	v = (terr,stop,id)
	myNext=[]
	y=float('inf')
        for r in run_query(u,v):
		myNext = r[0:2]
		y = r[2]
                break


	v = (eU.swap_gender(terr),stop,id)
	for r in run_query(u,v):
		if r[2]<y:
			myNext = r[0:2]
		break

	return [myPrevious, myNext]


def find_person(ID):
        s = "SELECT name||','||ID, name, ID FROM people WHERE ID=?"
        t = (ID,)

        for row in run_query(s,t):
                Self = row[0]
		selfName = row[1]
		selfID = row[2]
                return [Self, selfID,selfName]


def find_parents(ID):
        s = "SELECT name, parentID"\
                +" FROM parents LEFT JOIN people"\
                +" ON people.ID = parentID"\
                +" WHERE parents.ID = ?;"
        t = (ID,)

        parents = []
        parentIDs =[]
	parentNames=[]

        for row in run_query(s,t):
                if row[0]!=None:
                        p = row[0] + ',' + str(row[1])
                        pID = int(row[1])
			pN = row[0]
                else:
                        p = row[1] + ',p' + str(ID)
                        pID = 0
			pN = row[1]
                parents.append(p)
                parentIDs.append(pID)
		parentNames.append(pN)

	if len(parents)>1 and parents[1]==parents[0]:
		parents[1] = parents[1] + ' 2'

        return [parents,parentIDs,parentNames]



def is_married(IDa, IDb):
	
	s = "SELECT idb FROM marriages WHERE ida =?;"
	t = (min(IDa,IDb),)

		
	for row in run_query(s,t):
		if int(row[0])==max(IDa,IDb):
			return 1
	return 0	

def find_spouses(ID):
        t = (ID,)


        spouses = []
        spousesID=[]
	spousesNames=[]
	spouseDates = []

	s = "SELECT ida,a.name,idb,b.name,marriage,end,"\
	+" a.bornYear,a.diedYear,b.bornYear,b.diedYear,marriageYear,"\
	+" a.diedMonth,a.diedDay,b.diedMonth,b.diedDay"\
	+" FROM marriages"\
	+" LEFT JOIN people AS a"\
	+" ON ida = a.ID"\
	+" LEFT JOIN people AS b"\
	+" ON idb = b.ID"\
	+" WHERE ida = ? OR idb = ?"\
	+" ORDER BY marriageYear;"\

	t = (ID,ID)
	for row in run_query(s,t):
		sID = ''
		if row[0]!=int(ID): #spouse is ida
			if row[1]!=None:
				s = row[1]+","+str(row[0])
				sID = row[0]
				sN = row[1]
			elif row[0]!='':
				s = row[0]+",s"+str(ID)
				sID = 0
				sN = row[0]
			myDates = [row[1],row[0],row[4],row[5],row[6],\
			row[7],row[11],row[12]]
		else: #spouse is idb
			if row[3]!=None:
				s = row[3]+","+str(row[2])
				sID = row[2]
				sN = row[3]
			elif row[2]!='':
				s = row[2]+",s"+str(ID)
				sID = 0
				sN=  row[2]	
			myDates = [row[3],row[2],row[4],row[5],row[8],row[9],\
				row[13],row[14]]
			for i in range(len(myDates)):
				if myDates[i] is None:
					myDates[i]=0
		if sID!='':
			spouses.append(s)
			spousesID.append(sID)
			spousesNames.append(sN)
			spouseDates.append(myDates)

        return [spouses,spousesID,spousesNames,spouseDates]
	

def find_children(ID):
        s = "SELECT p1.name, p1.ID,p3.parentID,p4.name,p1.bornYear"\
                +" FROM people p1"\
                +" INNER JOIN parents p2"\
                +" ON p1.ID = p2.ID"\
                +" INNER JOIN parents p3"\
                +" ON p1.ID = p3.ID"\
                +" LEFT JOIN people p4"\
                +" ON p3.parentID = p4.ID"\
                +" WHERE p2.parentID = ?"\
                +" AND p3.parentID<>?"\
                +" ORDER BY p1.bornYear,p1.ID;"

        t = (ID,ID)

	childrenBorn=[]
	nodes=[]
	IDs=[]	
	names=[]

        for row in run_query(s,t):
                c = row[0] + ',' + str(row[1])
                cID = row[1]
		cName = row[0]
		born = row[4]
                childrenBorn.append(born)
		if row[3]!=None:
                        op = row[3] + ',' + str(row[2])
                        opID = row[2]
			opN = row[3]
                else:
                        op = row[2] + ',s' + str(ID)
                        opID = 0
			opN = row[2]

		nodes.append([c,op])
		IDs.append([cID,opID])
		names.append([cName,opN])

        return [nodes,IDs,names,childrenBorn]

def person_info(personID,newLine):
	t = (personID,)

	if newLine=='<br>':
		startP = '<p>'
		endP = '</p>'
	else:
		startP = ''
		endP = newLine

	mainDiv = ''	
	#Id, Name, Dates, Style, Style-Dates
	s = "SELECT id,name,fullname,url,picture,Born,Died,"\
		+" bornYear,diedYear,diedMonth,diedDay"\
		+" FROM people WHERE ID = ?"
	rows = 0
	for row in run_query(s,t):
		rows = 1
		mainDiv += startP
		preURL = pU.name_html(['previous',row[0]-1],newLine)
		postURL =  pU.name_html(['next',row[0]+1],newLine)
		mainDiv += 'ID: %s(%s %s)%s' %(row[0],preURL,postURL ,newLine)
		mainDiv += pU.print_tagged_name('Name',[row[1], row[0]]\
			,newLine)
		mainDiv+='Full Name: '+row[2]+newLine
		mainDiv +=endP

		name = row[1]
		fullname = row[2]
		url = row[3]
		picture = row[4]
		born = row[5]
		died = row[6]
		bornYear = row[7]
		diedYear = row[8]
		diedMonth = row[9]
		diedDay = row[10]

		mainDiv += startP
		mainDiv += '%sBorn:%s%s '% (newLine,born,newLine)



		if died!='present':
			mainDiv +='Died: %s' % died
	
			if diedYear != 0 and bornYear !=0:

				u = "SELECT age FROM"+ageTable\
					+" WHERE id = ?"
				
				for row in run_query(u,t):
					mainDiv += ", aged %s" %(row[0])
		else:
			u = "SELECT age FROM" + ageNowTable\
				+" WHERE id = ?"

			for row in run_query(u,t):
				mainDiv +='Still Alive, aged %s' %(row[0])

			

		mainDiv +=endP

	s = 'SELECT family FROM families WHERE ID = ?'
	
	for row in run_query(s,t):
		mainDiv+='Family: %s' % pU.fam_html(row[0],newLine)

	if rows==0:
		return ''

	s = "SELECT * FROM styles WHERE ID = ?"
	for row in run_query(s,t):
		mainDiv += startP
		mainDiv +='%sStyle: %s%s'%(newLine,row[1],newLine)

		mainDiv += 'Territories:%s' % newLine

		w = "SELECT short FROM styleDecomp"\
		+" WHERE style = ?"

		for r in run_query(w,(row[1],)):
			[p,n]=find_adjacent(r[0],row[3],row[5],personID)
			if len(p)>0:
				mainDiv +='%s|'%(pU.name_html(p,newLine))

			mainDiv+=pU.terr_html(r[0],newLine,row[3],row[5])

			if len(n)>0:
				mainDiv+='|%s'%(pU.name_html(n,newLine))
			mainDiv+=newLine

		mainDiv +=  'From: '+row[2] + newLine
                mainDiv +=  'To: '+row[4]	

		mainDiv += endP




	mainDiv += startP
	mainDiv += endP

	#find parents

	[parents,parentIDs,parentNames] = find_parents(personID)
	mainDiv += startP
	for i in range(len(parents)):
		r = [parentNames[i],parentIDs[i]]
		mainDiv += pU.print_tagged_name('Parent',r,newLine)
	mainDiv += endP

	#find spouses

	[spouses,spousesID,spousesNames,spouseDates] = find_spouses(personID)

	mainDiv += startP

	for i in range(len(spouses)):
		r = [spousesNames[i],spousesID[i]]
		d = spouseDates[i][2:8]

		mainDiv += pU.print_tagged_name('Spouse',r,newLine)
		if d[0]!='.':
			mainDiv+='married: '+d[0]+newLine
		else:
			mainDiv+='marriage dates not yet recorded'\
			+ newLine
		if d[1]!='.':
			mainDiv+='marriage ended: '+d[1]+newLine
		elif d[0]!='.':
			dPrint = 1
			if d[3]<diedYear:
				y = pU.print_year(d[3])
				
				ot = 'their'
			elif d[3]==diedYear and diedYear!=presentYear:
				y = d[3]
				
				if d[4]<diedMonth:
					ot = 'their'
				elif d[4]>diedMonth:
					ot = 'own'
				else:
					if d[5]<diedDay:
						ot = 'their'
					elif d[5]>diedDay:
						ot = 'own'
					else:
						dPrint = 0
						mainDiv+='until both of their deaths in %s%s'\
							%(str(y),newLine)

			else:
				if diedYear==presentYear:
					dPrint = 0
					mainDiv+='still married%s'%(newLine)
				else:
					y = str(pU.print_year(diedYear))
					
					ot='own'
			if dPrint==1:
				mainDiv+='until %s death in %s%s' %(ot,y,newLine)

		if r[1]!=0:
			mainDiv +=''
			#mainDiv += \
			#	pU.relationship_html(personID,r[1],newLine)\
			#	+newLine
		else:
			mainDiv+=newLine

		marriageStyles = ''
		mStart = int(findYear.find_year(d[0]))
		mStop = int(findYear.find_year(d[1]))
		if mStop == 0:
			mStop = diedYear

		if mStart==0:
			continue


		ss = "SELECT styles.style, short,startyear,"\
		+" stopyear"\
		+" FROM styles INNER JOIN styledecomp"\
		+" ON styles.style = styledecomp.style"\
		+" WHERE  id = ?;"
		st = (spousesID[i],)

		for sr in run_query(ss,st):
			starty = int(sr[2])
			stopy = int(sr[3])
			
			if mStart>stopy:
				continue
			if mStop<starty and mStop != 0:
				continue


			if mStart>starty:
				fromy = mStart
			else:
				fromy = starty

			if mStop<stopy and mStop !=0:
				to = str(mStop)
			elif diedYear<=stopy:
				to = str(pU.print_year(diedYear))
				
			else:
				to = str(sr[3])


			marriageStyles+='%d to %s: %s%s' \
			%(fromy, to,eU.swap_gender(sr[1]),newLine)

		if len(marriageStyles)>0:
			mainDiv+="Through this marriage:"+newLine
			mainDiv+=marriageStyles+newLine

	mainDiv  = mainDiv + newLine+endP

	#find children
	[nodes,IDs,names,childrenBorn] = \
		find_children(personID)

	top = ''
	for i in range(len(nodes)):
		cr = [names[i][0],IDs[i][0]]
		thisChild = pU.print_tagged_name('Child',cr,newLine)

		opr=[names[i][1],IDs[i][1]]
		top = names[i][1]
		if i==0 or  top != names[i-1][1]:
			mainDiv +=endP
			mainDiv += startP
			mainDiv += pU.print_tagged_name\
                        ('With',opr, newLine)


		#age when child born
		cb = childrenBorn[i]
		if  cb!=0 and  bornYear != 0:

			cs = "SELECT age,pID,cID FROM "+ageChildTable\
				+"WHERE pID=? AND cID =?"
			ct = (personID,IDs[i][0])

			for row in run_query(cs,ct):
				thisChild = thisChild[:-4]+\
				" at the age of "+str(row[0]) +newLine
		mainDiv += thisChild
	
	mainDiv += endP


	if newLine == '<br>':
		output = '<div id = "main" style = " float:left;width:75%">';
		output += mainDiv +  "</div>"

		output += "<div id = 'image' "\
			+"style = 'float:left; margin-left:20px'>"

		imageDiv = ''
		if picture!='.':
        		imageDiv += "<a href=" + url+">"\
                	+"<img src=" + picture +" alt = 'wiki link'"\
                	+" title = 'wiki link'></a>"\
                	+ newLine

     		elif url!='.' and url!='. ':
          		imageDiv += "<a href=" + url +">"\
         		+name + " (wiki link)</a>"+newLine

		output += imageDiv + "</div>"


		url = 'http://www.chiark.greenend.org.uk/ucgi/~naath/'\
			+'smallGraph.py'

		form = ''
		form += "<form id ='controlForm'"\
		+" action ="+ url +" method = 'get'>"

		form +=\
			"<input type = 'hidden' name = 'ID' value = "\
			+personID + "><br>"

                form +=\
		"Generations of Parents: "\
		+"<input type = 'text' name = 'pl' value='1'>"
		form += newLine
		form += \
		"Generations of Children: "\
		+" <input type = 'text' name = 'cl' value = '1'>"
		form += newLine
                form += \
                "Show siblings: <select name = 's'>"+\
                "<option value = '0'>No</option>"+\
                "<option value = '1'>Yes</option>"+\
		"</select>"
		form += newLine
                form += \
                "Show spouse's other spouses: <select name = 'os'>"+\
                "<option value = '0'>No</option>"+\
                "<option value = '1'>Yes</option>"+\
                "</select>"
		form += newLine
                form += \
                "Show parents' other spouses: <select name = 'pos'>"+\
                "<option value = '0'>No</option>"+\
                "<option value = '1'>Yes</option>"+\
                "</select>"		
		form += newLine
                form += \
		"Fount size: "+\
                "<input type = 'text' name = 'fs' value='8'>"
                form += newLine
        	form += "</form>"

		graph =  "smallGraph.py?ID="+str(personID)+"&fs=8"

		graph = "<img src ="+ graph + '>'

		output += "<div id = 'graph' style = 'clear:both'>"
		output += "<p id = 'agraph'>"+graph+"</p>"
		output += "Draw this graph with more relatives:"
		output += newLine + form
		
		output += "<button onclick='myFunction()'>"+\
			"Go</button>"

		output += "</div>"

		output +=\
		'<script>'+\
		'function myFunction()'+\
		'{'+\
		'var x = document.getElementById("controlForm");'+\
		'var txt = "<img src = " + x.action + "?";'+\
		'for (var i=0;i<x.length;i++)'+\
		'{'+\
		'var n=x.elements[i].name;'+\
		'var v=x.elements[i].value;'+\
		'txt = txt + "&"+n+"="+v;'+\
		'}'+\
		'txt = txt + ">";'+\
		'document.getElementById("agraph").innerHTML=txt;'+\
		'}'+\
		'</script>'

	else:
		output = mainDiv

	return output

def connect():
	global conn
	conn = sqlite3.connect\
		('/home/naath/familyTreeProject/familyTree/tree.db')
	conn.cursor().execute('PRAGMA journal_mode = WAL')

def make_cursor():
	return conn.cursor()

def commit_changes():
	conn.commit()
	
def close():
	conn.close

