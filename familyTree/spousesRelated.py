#!/usr/bin/python

import sys
import re
sys.path.append('/home/naath/familyTreeProject/cgiFiles')
import askQuestion as aQ
import graphQuestions as gQ
import printUtils as pU
import englishUtils as eU
import everyPage
import cgi

[conn,form]=everyPage.top()


conn = aQ.connect()

s = "select * from marriages ORDER BY ida;"
printMe=''
nPeople = aQ.number_people()

th = '<th>'
eth = '</th>'
td = '<td>'
etd = '</td>'


#printMe=pU.table_header(['person 1','ID1','person 2','ID2','relationship','year of marriage'],'<br>')
printMe=pU.table_header(['person 1','person 2','relationship','year of marriage'],'<br>')

cuz = re.compile('(<a.*?</a>) and (<a.*?</a>) are (.*)')
parent = re.compile('(<a.*?</a>) is (<a.*?</a>)\'s (.*)')
for row in aQ.run_query(s,()):
	if not eU.is_number(row[1]):
		continue

	ID1=int(row[0])
	ID2=int(row[1])
	if ID2>nPeople or ID1>nPeople or  ID2==0:
		continue

	relationship = row[-1]
	
	if relationship=='0':
		relationship = aQ.find_relationship(ID1,ID2,0)
	
	if relationship[-15:]!='are not related':
		print relationship
		marriedYear = pU.print_year(row[4])
		

		splitR = cuz.search(relationship)
		if splitR is None:
			splitR = parent.search(relationship)
#need to get ID numbers out of the URL because they are sometimes reversed
#should be a simple regex		
#		cols = [splitR.group(1),ID1,splitR.group(2),\
#			ID2,splitR.group(3),marriedYear]
		cols = [splitR.group(1),splitR.group(2),\
			splitR.group(3),marriedYear]

		printMe+=pU.table_row(cols,'<br>')
		
		
printMe+=pU.table_foot('<br>')
aQ.commit_changes()

aQ.close()

everyPage.good(printMe)
file = '/home/naath/public-html/spouses-related.html'
everyPage.to_file(file)
print 'done'


