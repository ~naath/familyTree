#!/usr/bin/python

s = 'CREATE TABLE people\n(\nID int,\nName text,\nFirstName'\
	+' text,\nBorn text,\nbornYear int,\nDied text,'\
	+'\ndiedYear int,\nbornMonth int,\ndiedMonth int,'\
	+'\nURL text,\nPicture text,\nPreName text,\nPostName text,'\
	+'\nfullName text, \nbornDay int,\ndiedDay int);'

print s

s = 'CREATE UNIQUE INDEX id_index ON people (ID);'

print s

s = 'CREATE TABLE names\n(\nID int,\nName text);'

print s

s = 'CREATE TABLE styles\n(\nID int,\nStyle text,\nStart text,\nstartYear int'\
	+',\nStop text,\nstopYear int,\nstartDate date,\nstopDate date);'

print s

#s = 'CREATE TABLE territories\n(\nID int,\nTerritory text,'\
#	+'\nStart text,\nstartYear,\nStop text,\nstopYear);'
#
#print s

s = 'CREATE TABLE parents\n(\nID int,\nparentID text);'

print s

s = 'CREATE TABLE families\n(\nID int, \nfamily text);'

print s

s = 'CREATE INDEX pid_index ON parents (ID);'

print s

s = 'CREATE INDEX sid_index ON styles (ID);'

print s

