#!/usr/bin/python

import sys
import re
sys.path.append('/home/naath/familyTreeProject/cgiFiles')
import askQuestion as aQ
import graphQuestions as gQ
import printUtils as pU
import englishUtils as eU
import everyPage
import cgi

#cgitb.enable()
[conn,form]=everyPage.top()

[a,b,c,d] = aQ.list_page('<br>')

everyPage.good(a)
everyPage.bottom()
file = '/home/naath/public-html/links.html'
everyPage.to_file(file)

everyPage.top()
everyPage.good(b)
everyPage.bottom()
file = '/home/naath/public-html/links-matched.html'
everyPage.to_file(file)

everyPage.top()
everyPage.good(c)
everyPage.bottom()
file = '/home/naath/public-html/links-unmatched.html'
everyPage.to_file(file)

everyPage.top()
everyPage.good(d)
everyPage.bottom()
file = '/home/naath/public-html/links-check.html'
everyPage.to_file(file)
