#!/usr/bin/env python

filename = 'Family Tree.csv'
targetfile = 'tree'

file = open(filename,'r')
tfile = open(targetfile,'w')

hrow = file.readline()
headings = hrow.split(';')

spIndex = headings.index('Spouses')
stIndex = headings.index('Style')
for line in file:
	array = line.split(';')
	print array[0]
	if len(array)<5:
		break

	if array[2]=='':
		break


	for i in range(spIndex):
		heading = headings[i]
		
		tfile.write(heading+':\n')
		tfile.write(array[i]+'\n\n')

	
	if not array[spIndex]=='.':
		sArray = array[spIndex].split(',')
		msArray = array[spIndex+1].split(',')
		meArray = array[spIndex+2].split(',')

		tfile.write(headings[spIndex]+':\n')
		for i in range(len(sArray)):
			s = sArray[i]
			if len(msArray)>i:
				md = msArray[i]
				ed = meArray[i]
			else:
				md='.'
				ed='.'
			tfile.write(s+'\n')
			tfile.write(md+'\n')
			tfile.write(ed+'\n')
			tfile.write('\n')


	i = stIndex
	
	
	while i<len(array):
		if array[i]=='Consort':
			tfile.write('Consort of:\n')
			tfile.write(array[i+1]+'\n')
			i=i+2
		elif array[i]=='':
			break
		else:
			tfile.write('Style:\n')
			tfile.write(array[i]+'\n')

			tArray = array[i+1].split(',')
			tfile.write('Territories:\n')
			
			for t in tArray:
				if len(t)>0:
					while t[0]==' ':
						t = t[1:]
					tfile.write(t+'\n')
			tfile.write('\n')

			if len(array)>i+3:
				tfile.write('From:\n')
				tfile.write(array[i+2]+'\n')
				tfile.write('To:\n')
				tfile.write(array[i+3]+'\n')
		
			i=i+4

	tfile.write('\n\n')

file.close()
tfile.close()
	
