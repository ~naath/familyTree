#!/usr/bin/python
import askQuestion as aQ
import englishUtils as eU
from string import Template
import re

global presentYear
presentYear=1000000

def table_header(columns,newLine):

	if newLine=='\n':
		printMe = ''
		for c in columns:
			printMe+=c+'	'
		return printMe
	th = '<th>'
	eth = '</th>'
	

	printMe = '<table class="sortable">'
	printMe+='<tr>'
	for c in columns:
		printMe+=th+c+eth
	
	printMe+='</tr>'

	return printMe

def table_row(row,newLine):
	if newLine=='\n':
		printMe=''
		for c in row:
			printMe+=str(c)+'	'
		return printMe
	td = '<td>'
	etd = '</td>'
	printMe = '<tr>'
	for c in row:
		printMe+=td+unicode(c)+etd
	printMe+='</tr>'
	return printMe

def table_foot(newLine):
	if newLine =='<br>':
		return '</table>'

def print_year(year):
	year = int(year)
	if year==0:
		return '?'
	if year==presentYear:
		return 'present'
	return year

def print_row(row,newLine):
	out = ''
	for item in row:
		out += str(item)+'|'
	return out[:-1] + newLine

def print_query(s,t,newLine):
	printMe = ''
	for row in aQ.run_query(s,t):
		printMe += print_row(row,newLine)		
	return printMe

def print_tagged_query(relationship,s,t,newLine):
	mine = ''
	for row in aQ.run_query(s,t):
		mine += print_tagged_name(relationship,row,newLine)
	return mine

def relationship_html(ID,ID2,newLine):
	ID = int(ID)
	ID2 = int(ID2)
	
	relationship = aQ.relation_text(ID,ID2,newLine)
		
	if newLine=='<br>' and relationship[-11:] != 'not related':

		script = "ancestors.py?ID=%s&ID2=%s" % (ID,ID2)
		url = link_Template.substitute\
			(script = script,title = "Common ancestors"\
				,text = "Common ancestors")
		return relationship + ' '+url + newLine
		
	else:
		return relationship+newLine

def terr_html(terr,newLine,start,stop):
	if newLine=='<br>':
		myTitle = eU.add_quotes(terr)

		terrURL=re.sub(' ','%20',terr)

		return link_Template.substitute(\
			script = "territory.py?terr="+terrURL, title=myTitle,\
			text = terr)
	else:
		return terr

def fam_html(fam,newLine):
	if newLine=='<br>':
		return link_Template.substitute(\
			script = "family.py?fam="+re.sub(' ','%20',fam),\
			title = eU.add_quotes(fam), text = fam)

def name_html(row,html):
	if html=='<br>':
		html=1
	elif html=='\n':
		html=0

	if row[0] == None:
		return row[1]
	elif row[1]==0:
		return row[0]
	else:
		if html==1:
			script = "person.py?ID=%s" %(row[1])
        	        name = row[0]
	                return link_Template.substitute(script = script\
				,title = eU.add_quotes(name),text = name)
		else:
			return "%s,%s" % (row[0],row[1])

def print_age_child_count(row,newLine):
	if newLine == '<br>':
		script = "listAge.py?age=%s" % (row[0])
		link = link_Template.substitute(script = \
			script, title = eU.add_quotes(row[0]), text = row[0])

		out = '%s %s had children at age %s %s'\
		 % (row[1],eU.print_people(row[1]),link,newLine)
		return out
	else:
		return print_row(row,newLine)

def print_age_death_count(row,newLine):
	if newLine =='<br>':
		script = "listAgeDeath.py?age=%s" % (row[0])
		if row[0] != presentYear:
			link = link_Template.substitute(script = script,\
				title = eU.add_quotes(row[0]),text = row[0])
			text = 'died at age'
		else:
			link = link_Template.substitute(script = script,\
				title = 'still alive',text = 'still alive')
			text = 'are'
		out = "%s %s %s %s %s " \
			%(row[1],eU.print_people(row[1]),text, link,newLine)
		return out
	else:
		return print_row(row,newLine)

def print_name_count(row,r,c,newLine):
	if newLine=='<br>':
		script = "name.py?name=%s" % (row[0])
		title = eU.add_quotes(row[0])
		link = link_Template.substitute(script =\
			script, title = title,text = row[0])
		
		newRow = [link,row[1],row[2],row[3],r,c]

		out=table_row(newRow,newLine)

		return out

#		return "%s %s called %s%s"\
#			%(row[1],eU.print_people(row[1]),link, newLine)
	else:
		return print_row(row,newLine)	

def print_children_count(row,newLine):
	out = "%s %s had " % (row[0],eU.print_people(row[0]))

	if newLine == '<br>':
		script = "listChildCount.py?nc="+str(row[1])
		link = link_Template.substitute(script =\
			script, title = eU.add_quotes(row[1]),text = row[1])
	else:
		link = str(row[1])

	out += "%s %s %s" % (link,eU.print_children(row[1]),newLine)

	return out

def print_tagged_name(relationship,row,newLine):
	if row[0]==None:
               	out = relationship + " not yet entered: " + row[1]
		out = "%s not yet entered: %s" % (relationship,row[1])
       	else:
		if newLine == '<br>':
			html = 1
		else:
			html=0
		if relationship =='':
			out = '%s   ' % (name_html(row,html))
		else:
	                out = relationship + ": " + name_html(row,html)
			out = '%s: %s' % (relationship,name_html(row,html))
	return out + newLine



global link_Template 
link_Template= Template(\
	"<a href = http://www.chiark.greenend.org.uk/ucgi/~naath/$script"\
	+" title=$title>$text</a>")
