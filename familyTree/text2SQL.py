#!/usr/bin/python
import findYear
import re

def add_quotes(s):
	return '\"'+s+'\"'

def is_number(s):
    try:
        float(s)
        return s
    except ValueError:
        return add_quotes(s)

def make_insert(table,fields):
	s = 'INSERT INTO ' + table + ' VALUES('
	for f in fields:
		s = s + str(f) + ','
	s = s[:-1]
	s = s+');'
	return s

f = open('tree','r')

lastline='';
finishedRecord = 0;
hasStyle = 0;
terr = 0;
for line in f:
	thisline = line
	thisline = thisline[:-1]
	
	if thisline[-2:] == '\r\n':
		thisline = thisline[:-2]
	if lastline == 'ID:':
		thisID = thisline

	if lastline=='Pre-name style:':
		prens = thisline

	if lastline=='Post-name style:':
		postns=thisline

	if lastline == 'Name:':
		a = re.search(' ([A-Z]+[^a-z])',thisline)
		names = thisline.split()

		if a !=None:
			name = a.group(1)
			firstName = name[0] + name[1:].lower()
		else:
			if len(names)>0:
				firstName = names[0]
			else:
				firstName = ''

		thisName = add_quotes(thisline)
	if lastline == 'Born:':
		yb = findYear.find_year(thisline)
		mb = findYear.find_month(thisline)
		thisBorn =  add_quotes(thisline)
	if lastline == 'Died:':
		yd = findYear.find_year(thisline)
		md = findYear.find_month(thisline)
		thisDied =  add_quotes(thisline)
		finishedRecord=1
	if lastline == 'URL:':
		url = add_quotes(thisline)
	if lastline == 'Picture:':
		picture = add_quotes(thisline)
	if lastline == 'Father:':
		a = is_number(thisline)
		s = make_insert('parents',[thisID, a])
		print s
	if lastline=='Mother:':
		a=is_number(thisline)
		s = make_insert('parents',[thisID, a])
                print s
	
	if finishedRecord ==1:
		if prens!='.':
			pre = prens+' '	
		else:
			pre = ''
		if postns!='.':
			post = ' '+postns
		else:
			post = ''
		titleName =''
		if pre!='':
			titleName += pre + ' '
		titleName += firstName
		if post!='':
			titleName+=post
		titleName = add_quotes(titleName)
			
		prens = add_quotes(prens)
		postns = add_quotes(postns)
		firstName = add_quotes(firstName)
		s = make_insert('people',\
		[thisID,titleName,firstName,thisBorn,yb,\
			thisDied,yd,mb,md,url,picture,prens,postns,thisName])
		print s
		finishedRecord = 0
	if lastline == 'Style:':
		thisStyle =  add_quotes(thisline)
		hasStyle = 1;
	if terr ==1:
		if thisline=='':
			terr=0;
		else:
			thisTerr.append(add_quotes(thisline))
	if lastline == 'Territories:':
		thisTerr=[add_quotes(thisline)]
		terr = 1;
	if hasStyle == 1:
		if lastline=='From:':
			yf = findYear.find_year(thisline)
			thisFrom =  add_quotes(thisline)
		if lastline =='To:':
			yt = findYear.find_year(thisline)
			thisTo =  add_quotes(thisline)
			s = make_insert('styles',[thisID,thisStyle,thisFrom,\
				yf,thisTo,yt])
			print s

			for terr in thisTerr:
				s = make_insert('territories',[thisID,terr,\
					thisFrom,yf,thisTo,yt])

				print s

			hasStyle = 0

	lastline = thisline


