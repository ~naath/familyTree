#!/usr/bin/python

import re

def find_year(date):
	if date == 'present':
		return 1000000

	dates = date.split('/')

	if len(dates)>0:
		year = dates[-1]
	else:
		year = 0

	parts = year.split('-')

	matches = re.search('[0-9]{1,4}',parts[0])
	if matches==None:
		year =  0
	else:
		year =  matches.group(0)

	return year


def find_month(date):

	dates = date.split('/')
	if len(dates)==3:
		return dates[1]
	if len(dates)==2:
		parts = date.split('-')
		matches = re.search('[0-9]{1,2}',parts[0])
		if matches == None:
			return 0
		else: 
			return matches.group(0)
	else:
		return 0

def find_day(date):
	dates = date.split('/')
	if len(dates)==3:
		parts = dates[0].split('-')
		matches = re.search('[0-9]{1,2}',parts[0])
		if matches == None:
			return 0
		else:
			return matches.group(0)

	else:
		return 0

def reverse_order(date):

	year = find_year(date)
	if year==0:
		year = '0000'
	month = find_month(date)
	if month==0:
		month='00'
	day = find_day(date)
	if day==0:
		day = '00'

	return '%s-%s-%s' %(year,month,day)
