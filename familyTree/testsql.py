#!/usr/bin/python

import askQuestion as aQ
import sys	
import re
import string
def add_quotes(s):
        return '\"'+s+'\"'

def make_insert(table,fields):
        s = 'INSERT INTO ' + table + ' VALUES('
        for f in fields:
                s = s + add_quotes(f) + ','
        s = s[:-1]
        s = s+');'
        return s



conn = aQ.connect()

s = 'CREATE TABLE test\n(\nStyle text,\nShort text);'
a = aQ.run_query(s,())

for i in range(0,10):
	s = make_insert('test',[str(i),'a'])
	a = aQ.run_query(s,())
	conn.commit()
aQ.close(conn)
