#!/usr/bin/python

import cgi
#import cgitb
#cgitb.enable()
import sys
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion as aQ
import graphQuestions as gQ
import make_dot as d
from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
import pygraph.algorithms.minmax as minmax
import pygraph.algorithms.accessibility as access
import pickle


def make_graph2(rulers):
	#old version, slow.
	myNodes = []

	for i in range(len(rulers)):
		ruler = rulers[i]
		if ruler not in myNodes:
			myNodes.append(ruler)
			path = None
			j = -1

			while  j>-len(rulers):
				top = rulers[j]
				path = gQ.is_descendant(top,ruler)
				if not path is None and len(path)>1:
					myNodes+=path
					break
				j = j-1
			if path is None:
				j = i+1
				if j>len(rulers)-1:
					j = i-1
				while j>0:
					pre = rulers[j]
					mrca = gQ.relationship(ruler,pre)[0]
					if len(mrca)>0:
						path = \
						gQ.is_descendant(mrca[0],ruler)
						myNodes+=mrca
						myNodes+=path
						path = \
						gQ.is_descendant(mrca[0],pre)
						myNodes+=path
						break
					if j>i:
						j=j+1
					else:
						j=j-1
					if j+1>len(rulers):
						j = i-1
	draw_graph(myNodes)

def make_graph(rulers):

	myNodes = gQ.join_up(rulers,rulers)

	draw_graph(myNodes)

def draw_graph(myNodes):
	myNodes = set(myNodes)
	myNodes = list(myNodes)
	d.start_dot(6)

	for r in rulers:
		ruler = aQ.find_person(r)[0]
		d.add_highlight(ruler)
	for r in rulers:
		ruler = aQ.find_person(r)[0]
		spouses = aQ.find_spouses(r)[0]
		for s in spouses:
			d.add_marriage(ruler,s,[],1)

	for n in myNodes:
		node = aQ.find_person(n)[0]
		parents,pIDs,pNs = aQ.find_parents(n)
		d.add_person(node)
#		if int(pIDs[0]) in myNodes or int(pIDs[1]) in myNodes:
		if 1==1:
			d.add_marriage(parents[0],parents[1],[node],1)	

	d.create_dot()
	d.add_subgraphs()
        d.end_dot()

	if web ==1:
	        d.render_dot()
	
global web
web = 1
form = cgi.FieldStorage()
if web==1:
	terr = form.getvalue('terr')
else:
	terr = "King of Great Britain"
#	terr = None
#	rulers = [40,42]

conn = aQ.connect()
if terr != None:
	if terr[0]=="'":
		terr = terr[1:-1]
	rulers = aQ.find_rulers(terr,'<br>')[1]
else:
	global rulersStr
	rulersStr = form.getvalue('names')
	rulers=[]	
	if rulersStr!=None:
		rulersStr = rulersStr.split(',')
		for r in rulersStr:
			rulers.append(int(r))
make_graph(rulers)
aQ.close()

