#!/usr/bin/python
# -*- coding:utf-8 -*-
import cgi  
import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
from random import randrange
import codecs

cgitb.enable()
def base_url():
	return 'http://www.chiark.greenend.org.uk/ucgi/~naath/'

def make_list(script,text):
	global pageText
	if text == 'Random person':
		npeople = askQuestion.number_people()
		script+='?ID=%d' %(randrange(npeople))

	if script[:4]=='http':
		pageText+= '<li><a href=%s>%s</a></li>\n' %(script,text)
	else:
		pageText+= '<li><a href = %s%s>%s</a></li>\n' %(base_url(),script,text)

def links():
	global pageText
	items = []
	items.append(('searchname.py','Search for people'))
	items.append(('listPeople.py','List of people'))
	items.append(('listTerr.py','List of territories'))
	items.append(('listFam.py','List of families'))
	items.append(('http://www.chiark.greenend.org.uk/~naath/bigGraph.png',\
		'Big Graph'))
	items.append(('countNames.py','First name'))
	items.append(('listAge.py','Age at having child'))
	items.append(('listAgeDeath.py','Age at death'))
	items.append(('listChildCount.py','Number of children'))
	items.append(('birthday.py','Birthday calendar'))
	items.append(('person.py','Random person'))
	items.append(('aliveOn.py','Alive on date'))
	items.append((\
	'http://www.chiark.greenend.org.uk/~naath/spouses-related.html'\
		,'Spouses Related'))
	items.append((\
	'http://www.chiark.greenend.org.uk/ucgi/~naath/git/familyTree.git'\
		, 'source code'))
	pageText+= '<hr>\n'
	for i in range(len(items)):
		item = items[i]
		if i%4==0 and i!=len(items):
			if i!=0:
				pageText+= '</ul>\n'
			pageText+= '<ul>\n'
		make_list(item[0],item[1])
	pageText+= '</ul>\n'
	pageText+= '<hr>\n'



	
def footer():
	global pageText
	pageText+= '<hr>\n'
	pageText+= 'This somewhat silly thing made by \n'
	pageText+= '<a href=http://www.chiark.greenend.org.uk/~naath>naath</a>\n'
	pageText+= '<br>\n'
	pageText+= 'Thanks to <a href=http://www.chiark.greenend.org.uk>chiark</a> for hosting this\n'
	pageText+= '<br>\n'
	pageText+= 'Information sourced from <a href = en.wikipedia.org> wikipedia</a>\n'
	pageText+= 'All errors in transcription are mine.\n'
	pageText+= 'Reports of errors, or ideas of interesting questions to ask\n'
	pageText+= 'my database by email to naath@chiark.greenend.org.uk\n'
	pageText+= "(omissions of people are largely because I haven't got to them yet).\n"



def title(titleNum):
	return 'Silly toy'

def page_header():
	global pageText
	pageText+= 'This is a silly toy for exploring the genealogy of the monarchs of England'

def top():
	global pageText

	pageText= "Content-Type: text/html;charset=utf-8\n"
	pageText+= "\n "

	conn = askQuestion.connect()

	form = cgi.FieldStorage()
	return [conn,form]

def html_start(titleNum):
	global pageText
	pageText+= "<html>\n"
	pageText+='<meta charset="UTF-8" />\n'
        pageText+= "<head>\n"
        pageText+= "<title>"+ title(titleNum) +"</title>\n"

	pageText+= "<style>\n"
	pageText+= "ul\n{list-style-type:none;margin:10;padding:0;}\n"
	pageText+= "li{display:inline;}\n"
	pageText+= "</style>\n"
	pageText+= '<script type="text/javascript"'
	#myScript = '"/home/naath/public-html/sorttable.js"'
	myScript = 'http://www.chiark.greenend.org.uk/~naath/sorttable.js'
	pageText+= ' src='+myScript+'></script>'


        pageText+= "</head>\n"
        pageText+= "<body>\n"
	page_header()
	links()

def bad():
	global pageText
	html_start(1)
	pageText+= 'Go Away\n'

def good(printMe):
	global pageText
	html_start(2)
	pageText+= printMe

def html_bottom():
	global pageText
	links()	
	footer()
	pageText+= "</body>\n"
        pageText+= "</html>\n"
	askQuestion.close()

def bottom():
	global pageText
	html_bottom()

	print pageText.encode('utf-8')
def to_file(file):
	global pageText
	html_bottom()
	pageText = pageText[39:]

	f = codecs.open(file,mode = 'w',encoding = 'utf-8')
        f.write(pageText)
        f.close

global pageText
pageText = ''

