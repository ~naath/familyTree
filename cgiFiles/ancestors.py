#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
import everyPage

[conn,form]=everyPage.top()

ID = form.getvalue('ID')
if ID == None:
        ID = 1

ID2 = form.getvalue('ID2')
if ID2 == None:
	type =1
else:
	type =2

result = re.match('^[0-9]{1,4}$', str(ID))
if type==2:
	result2 = re.match('^[0-9]{1,4}$', str(ID2))
else:
	result2 = 1

if result == None or result2==None:
        everyPage.bad()
else:   
	if type==1:
	        printMe = askQuestion.all_ancestors(ID,'<br>')[0]
        
	else:
		printMe = askQuestion.common_ancestors(ID,ID2,'<br>')
	if len(printMe)<10:
                printMe =  'sorry, no data <br>'

        everyPage.good(printMe)


everyPage.bottom()
