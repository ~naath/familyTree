#!/usr/bin/python

import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
import everyPage

[conn,form]=everyPage.top()

terr = form.getvalue('terr')
if terr == None:
        terr = 'Wessex'
result = re.match('^[ A-Za-z-]{1,100}$', str(terr))


if result == None:
        everyPage.bad()
else:   
        printMe = askQuestion.rulers_of(terr,'<br>')
        if len(printMe)<1:
                printMe =  'sorry, no data <br>'

        everyPage.good(printMe)


everyPage.bottom()
