#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
import everyPage

#cgitb.enable()

[conn,form]=everyPage.top()

name = form.getvalue('name')
if name == None:
	name = "Edward"

result = re.match('[a-zA-Z\.\']*$', name)

if result == None:
	everyPage.bad()
else:   
	printMe  = askQuestion.people_with_name(name,'<br>')
	if len(printMe)<10:
		printMe =  'sorry, no data <br>'

	everyPage.good(printMe)


everyPage.bottom()
