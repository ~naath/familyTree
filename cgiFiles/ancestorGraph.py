#!/usr/bin/python

import cgi
#import cgitb
#cgitb.enable()
import sys
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion as aQ
import make_dot as d


def add_parents(ID,Self):

	[parents, parentIDs,parentNames] = aQ.find_parents(ID)

	pair = []
	for i in range(len(parents)):
		newSelf = parents[i]
		if parentIDs[i]!=0:
			newID = parentIDs[i]
			if not d.has_node(newSelf):
				d.add_person(newSelf)
				add_parents(newID,newSelf)
		else:
			d.add_person(newSelf)
		pair.append(newSelf)

	d.add_marriage(pair[0],pair[1],[Self],1)

def make_graph(ID):
	global allAncestors

	d.start_dot(8)
		

	Self = aQ.find_person(ID)[0]
        d.add_highlight(Self)

	add_parents(ID,Self)
	d.create_dot()
	d.add_subgraphs()
        d.end_dot()
        d.render_dot()

form = cgi.FieldStorage()

ID = form.getvalue('id')

conn = aQ.connect()
make_graph(ID)
aQ.close(conn)

