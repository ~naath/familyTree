#!/usr/bin/python

import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
import everyPage

[conn,form]=everyPage.top()

date = form.getvalue('date')
if date == None:
        printMe = askQuestion.calendar('<br>')
        if len(printMe)<3:
                printMe =  'sorry, no data <br>'

        everyPage.good(printMe)

else:
	result = re.match('^[0-9]{1,2}[-][0-9]{1,2}$', str(date))

	if result == None:
		everyPage.bad()
	else:
		printMe  = askQuestion.born_on(date,'<br>')
        	if len(printMe)<10:
                	printMe =  'sorry, no data <br>'

       	 	everyPage.good(printMe)


everyPage.bottom()
