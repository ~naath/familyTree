#!/usr/bin/python

import cgi
#import cgitb
import gv
import re
import sys
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion as aQ
import englishUtils as eU

#cgitb.enable()
def add_quotes(s):
        return '\"'+str(s)+'\"'

def add_node(node,node_attr):
	na = attr_string(node_attr)
	if not has_node(node):
		nodes[node] = na
	elif node_attr[-2][1]==highlight_attr[0][1]:	
		nodes[node] = na

def has_node(node):
	if node in nodes.keys():
		return 1
	else:
		return 0
def has_edge(edge):
	edge[0] = add_quotes(edge[0])
	edge[1] = add_quotes(edge[1])
	if edge in edges.keys():
		return 1
	else:
		return 0

def add_edge(n1,n2,edge_attr):
	edge = (add_quotes(n1),add_quotes(n2))
	edge_attr = attr_string(edge_attr)
	if edge not in edges.keys():
		edges[edge] = edge_attr

def add_arrow(n1,n2):
	add_edge(n1,n2,edge_attr)

def add_no_arrow(n1,n2):
	add_edge(n1,n2,nodir_attr)

def add_marriage(n1,n2,children,joinChildren):
	global cNodes
	
	jN = n1+n2
	jNRev = n2+n1
	if has_node(jNRev):
		jN = jNRev
	add_spot(jN)
	add_person(n1)
	add_person(n2)


	if len(children)>0 and joinChildren==1:
		cNode = jN + 'c'
		tcNode = cNode + children[0]
		
		if cNodes.has_key(cNode):
			if not has_node(tcNode):
				if len(cNodes[cNode])>0:
					last = cNodes[cNode][-1]
				add_spot(tcNode)
				cNodes[cNode].append(tcNode)
				add_no_arrow(last,tcNode)
				add_subgraph([tcNode,last])
		else:
			add_spot(tcNode)
			cNodes[cNode] = [tcNode]
			add_no_arrow(jN,tcNode)


	elif len(children)>0:
		tcNode = jN

	for c in children:
		add_person(c)
		add_arrow(tcNode,c)

	m = 1
	good = 1
	try:
		id1 = int(n1.split(',')[-1])
		id2 = int(n2.split(',')[-1])

		m = aQ.is_married(id1,id2)
	except:
		m = 1

	for n in [n1, n2]:
		if m==1:
			add_edge(n,jN,marriage_attr)
		else:
			add_edge(n,jN,not_marriage_attr)
	add_subgraph([n1,n2])


def add_subgraph(myG):
	global subgraphs
	if myG not in subgraphs:
		subgraphs.append(myG)
	

def start_dot(fs):
	global nodes
	global edges
	global cNodes
	global dot
	global subgraphs

	nodes = dict()
	edges=dict()
	cNodes={}
	dot = "digraph G{\n"
	dot = dot + "ranksep = 0.2 nodesep = 0.1\n"
	subgraphs=[]
	set_attr(fs)

def create_dot():
	global dot

	for node in nodes.keys():
		dot +=add_quotes(node) + nodes[node] + ";\n"

	for edge in edges.keys():
		dot += edge[0] + ' -> ' + edge[1] + edges[edge] + ";\n"

def add_subgraphs():
	global dot
	for sg in subgraphs:
		line = "\n{rank=same "

                for n in sg:
                        line  +=add_quotes(n) + ' '

                line += "}"

		dot+=line

def end_dot():
	global dot
        dot = dot + "}"


def arrange_name(name):

	ln = name.replace('  ',' ')
        ln = ln.replace(', ','\\n')
        ln = ln.replace(',','\\n')
        ln = ln.replace(' ','\\n')

	return str(ln)

def add_highlight(name):
	try:    
                k=eU.isKing(name)
        except: 
                k=0

        myLabel =str(arrange_name(name))

        if k==1:
                add_node(name,king_attr+[('label',myLabel)]+highlight_attr)
        else:
                add_node(name,person_attr+[('label',myLabel)]+highlight_attr)	

def add_person(name):
	try:
		k=eU.isKing(name)
	except:
		k=0
	myLabel = str(arrange_name(name))

	if k==1:
		add_node(name,king_attr+[('label',myLabel)])
	else:
		add_node(name,person_attr+[('label',myLabel)])

def add_spot(name):
	add_node(name,spot_attr)


def set_attr(fs):
	global person_attr
        global highlight_attr
	global king_attr
        global spot_attr
        global edge_attr
        global invis_attr
        global ignored_attr
        global nodir_attr
	global marriage_attr
	global not_marriage_attr
	global debug_attr
	global debug_edge_attr
	zero_size = [('width','0'),('height','0'),('fontsize',str(fs))]
        person_attr = [('shape','plaintext'),('margin','0')] +\
		zero_size
	highlight_attr =  [('fillcolor','yellow'),('style','filled,bold')]
	king_attr = person_attr +\
		[('fontcolor','purple4'),('shape','house'),('color','purple4')]
	debug_attr = person_attr + [('fontcolor','green')]
	spot_attr = [('shape','point')] + zero_size
	edge_attr = [('len','0'),('arrowsize','0.5')]
	debug_edge_attr = edge_attr + [('color','green')]
	invis_attr = edge_attr + [('style','invis')]
	nodir_attr = edge_attr + [('dir','none')]
	marriage_attr = nodir_attr + [('weight','10'),('color','red')]
	not_marriage_attr = nodir_attr+[('weight','10'),('color','blue')]
	ignored_attr =edge_attr + [('constraint','false')] + nodir_attr

def attr_string(attr):
	
	attr_str = '['
	for a in attr:
		attr_str = attr_str + a[0] + '="' + a[1]+'"'
		if a != attr[-1]:
			attr_str = attr_str +','

	attr_str = attr_str + ']'
	return attr_str

def render_dot():

	f = open('tempfile','w')
	f.write(dot)
	f.close

	gvv = gv.readstring(dot)
        gv.layout(gvv,'dot')

        format = 'png'

        print "Content-type: image/" + format + "\n"
        print gv.render(gvv,format)


def render_to_file(file):
	f = open('/home/naath/familyTreeProject/cgiFiles/biggraphfile','w')
	f.write(dot)
	f.close
	gvv = gv.readstring(dot)
	gv.layout(gvv,'dot')
	format = 'png'
	gv.render(gvv,format,file)


global dot
global nodes
global edges
global subgraphs
global cNodes
