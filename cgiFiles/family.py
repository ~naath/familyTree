#!/usr/bin/python

import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
import everyPage

[conn,form]=everyPage.top()

fam = form.getvalue('fam')
if fam == None:
        fam = 'Wessex'
result = re.match('^[ A-Za-z-]{1,100}$', str(fam))


if result == None:
        everyPage.bad()
else:   
        printMe = askQuestion.people_in(fam,'<br>')
        if len(printMe)<1:
                printMe =  'sorry, no data <br>'

        everyPage.good(printMe)


everyPage.bottom()
