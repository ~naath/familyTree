#!/usr/bin/python

import cgi
#import cgitb
import make_dot as d
import sys
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion as aQ
#cgitb.enable()

def add_parents(ID,s,cl,pl,pos,os):
	Self = aQ.find_person(ID)[0]
	
	[parents, parentIDs,parentNames] = aQ.find_parents(ID)

	if s==0:
	        d.add_marriage(parents[0],parents[1],[Self],1)
	
	else:
		for p in parentIDs:
			if p!=0:
				add_children(p,cl+1,os)
	if pl>1:
		for p in parentIDs:
			if p!=0:
				add_parents(p,s,cl,pl-1,pos,os)

	if pos==1:
		for p in parentIDs:
			if p!=0:
				add_spouses(p,os,cl)


def add_children(ID,cl,os):
	Self = aQ.find_person(ID)[0]


	[nodes,IDs,names,childrenBorn] = \
		aQ.find_children(ID)
	
	children=[]
	for n in nodes:
                c = n[0]
		children.append(c)
                op = n[1]
                d.add_marriage(Self,op,[c],1)

        #d.subgraphs.append(children)

	if cl>1:
		for c in IDs:
			add_children(c[0],cl-1,os)
			add_spouses(c[0],os,cl-1)

def add_spouses(ID,os,cl):
	Self = aQ.find_person(ID)[0]

	[spouses,spousesID,spousesNames,sD] = aQ.find_spouses(ID)

	for s in spouses:
                d.add_marriage(Self,s,[],1)
	

	if os==1:
		for s in spousesID:
			if s!=0:
				add_spouses(s,0,cl)
				add_children(s,cl,os)


def make_graph(ID,v,fs):
	pl = v[0]
	cl = v[1]
	s = v[2]
	os = v[3]
	pos = v[4]

	d.start_dot(fs)	
	Self = aQ.find_person(ID)[0]

	d.add_highlight(Self)

	add_parents(ID,s,cl,pl,pos,os)
	add_children(ID,cl,os)
	add_spouses(ID,os,cl)

	d.create_dot()
	d.add_subgraphs()
	
	d.end_dot()
		
	d.render_dot()

def check_int(s):
	try:
		return int(s)
	except ValueError:
		return -1

form = cgi.FieldStorage()

ID = form.getvalue('ID')


variables = ['pl', 'cl', 's', 'os', 'pos']
values=[]
for s in variables:
	values.append(form.getvalue(s))

for i in range(len(values)):
	v = values[i]
	if v == None or v<0:
		v = 0
	v = check_int(v)
	values[i] = v

fs = form.getvalue('fs')
if fs==None:
	fs=8
fs = check_int(fs)
if fs<0:
	fs=8
if fs>20:
	fs=20

conn = aQ.connect()
make_graph(ID,values,fs)
aQ.close(conn)


