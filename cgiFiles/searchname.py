#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion as aQ
import everyPage


def make_drop_down(n, IDs, Names):
	if n==1:
		formName = 'ID'
	else:
		formName = 'ID' + str(n)	
	
	dd=\
                "Person "+str(n)+": <select name = "+formName + ">"
	for i in range(len(IDs)):
		dd = dd +\
		"<option value = '"+str(IDs[i])+"'>"+Names[i]+"</option>"
		 

	dd = dd+"</select>"

	return dd

#cgitb.enable()

[conn,form]=everyPage.top()

name = form.getvalue('name')
name2 = form.getvalue('name2')

thisName = ''
nameA=''
nameB=''
if name2==None and name !=None:
	thisName = name
elif name2!=None and name!=None:
	nameA = name
	nameB = name2	

oneSearch = "Search for people by name"+\
"<form id='searchBox' action = 'searchname.py' method = 'get'>"+\
"Search for: <input type = 'text' name = 'name' value =" +thisName+"> <br>"+\
"<button type = 'submit'>Search</button><br><br>"+\
"</form>"

pairSearch = "Search for pairs of people"+\
" to find joint ancestors"+\
"<form id='searchBox' action = 'searchname.py' method = 'get'>"+\
"Person 1: <input type = 'text' name = 'name' value =" +nameA+"> <br>"+\
"Person 2: <input type = 'text' name = 'name2' value = "+nameB+"> <br>"+\
"<button type = 'submit'>Search</button><br><br>"
"</form>"
	
if name == None:
	printMe = oneSearch + pairSearch
	
	everyPage.good(printMe)	

elif name2 ==None:
	result = re.match('[a-zA-z-% ]*$', name)

	if result == None:
		everyPage.bad()
	else:   
		printMe  = \
		aQ.search_name(name,'<br>')[0]
		if len(printMe)<10:
			printMe =  'sorry, no data <br>'

		printMe = printMe + '<br>' + oneSearch + pairSearch
		everyPage.good(printMe)


else:

	[out1,names1,IDs1] = aQ.search_name(name,'<br>')
	[out2,names2,IDs2] = aQ.search_name(name2,'<br>')

	printMe = \
	"<form id='chooseList' action = 'ancestors.py' method = 'get'>"+\
	make_drop_down(1,IDs1,names1)+\
	"<br>"+\
	make_drop_down(2,IDs2,names2)+\
	"<br><button type = 'submit'>Search</button><br><br>"+\
	"</form>"+\
	oneSearch+ pairSearch

	everyPage.good(printMe)

everyPage.bottom()

