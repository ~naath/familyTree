#!/usr/bin/python

import cgi
#import cgitb
#cgitb.enable()
import sys
sys.path.append('/home/naath/familyTreeProject/familyTree')
import make_dot as d
import askQuestion as aQ
import graphQuestions as gQ



def make_graph(ID,ID2):
        d.start_dot(8)	


	mrca,otherRCA,lA,lB = gQ.relationship(ID,ID2)

	nodes = gQ.join_up([ID,ID2],mrca+otherRCA)
	

	for node in nodes:
		[Self, selfID, selfName] = aQ.find_person(node)

		if node==ID or node==ID2 or node in mrca or node in otherRCA:
			d.add_highlight(Self)

		[ps,pIDs,pNames] = aQ.find_parents(node)
		if node not in mrca+otherRCA:
			d.add_marriage(ps[0],ps[1],[Self],1)
		elif (pIDs[0] in mrca+otherRCA) or (pIDs[1] in mrca+otherRCA):
			d.add_marriage(ps[0],ps[1],[Self],1)




	if web==0:
		return
	d.create_dot()
	d.add_subgraphs()
	d.end_dot()
	d.render_dot()
	aQ.close(conn)

global web
global maxLevel
web = 1
if web == 1:
	form = cgi.FieldStorage()

	ID = int(form.getvalue('id'))
	ID2 = int(form.getvalue('id2'))
	maxLevel = int(int(form.getvalue('mL')))
else:

	ID = 60
	ID2 = 226
	maxLevel = 3
conn = aQ.connect()
make_graph(ID,ID2)
aQ.close()

