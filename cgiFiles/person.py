#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
import everyPage

#cgitb.enable()

[conn,form]=everyPage.top()

ID = form.getvalue('ID')
if ID == None:
	ID = 1
ID = str(ID)

result = re.match('^[0-9]{1,4}$', str(ID))

if result == None:
	everyPage.bad()
else:   

	printMe  = askQuestion.person_info(ID,'<br>')
	if len(printMe)<10:
		printMe =  'sorry, no data <br>'
	else:
		url = "<a href= "+everyPage.base_url()\
		+"ancestors.py?ID="+str(ID)+">Ancestors</a>"
		url2 = "<a href="+everyPage.base_url()\
		+"descendants.py?ID="+str(ID)+">Descendants</a>"
		printMe = printMe + '<br>' + url+'<br>'+url2

	everyPage.good(printMe)
	
everyPage.bottom()
