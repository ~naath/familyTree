#!/usr/bin/python

import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
import everyPage

[conn,form]=everyPage.top()

nc = form.getvalue('nc')
if nc == None:
        printMe = askQuestion.count_children('<br>')
        if len(printMe)<3:
                printMe =  'sorry, no data <br>'

        everyPage.good(printMe)

else:   
        result = re.match('^[0-9]{0,4}$', str(nc))

        if result == None:
		everyPage.bad()
        else:   
                printMe  = askQuestion.parents_with_children(int(nc),'<br>')
                if len(printMe)<10:
                        printMe =  'sorry, no data <br>'

                everyPage.good(printMe)


everyPage.bottom()
