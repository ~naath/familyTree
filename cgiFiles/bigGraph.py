#!/usr/bin/python

#import cgi
##import cgitb
import sys
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion as aQ
import make_dot as d
from pygraph.classes.digraph import digraph
import pickle
import englishUtils as eU
import graphQuestions as gQ

def add_quotes(s):
        s = str(s)
        return '\"'+s+'\"'

def an(node):
	node = int(node)
	if not g.has_node(node):
		g.add_node(node)

def ae(edge):
	iEdge=(int(edge[0]),int(edge[1]))
	if not g.has_edge(iEdge):
		g.add_edge(iEdge)

global g
#cgitb.enable()

#form = cgi.FieldStorage()
conn = aQ.connect()


d.start_dot(8)
g = digraph()

everyone = 0
kings = []
kingsp = []
s = "SELECT id FROM people"
for row in aQ.run_query(s,()):
	sID = int(row[0])
	print sID
	self,sID,sN = aQ.find_person(sID)
	parents,pIDs,pNs = aQ.find_parents(sID)
	spouses,spIDs,spNs,spDs = aQ.find_spouses(sID)


	an(sID)

	if everyone==1:
		spouses,spIDs,spNs,spDs = aQ.find_spouses(sID)
		d.add_person(self)

		d.add_marriage(parents[0],parents[1],[self],0)
		d.add_subgraph(parents)
		for spouse in spouses:
			d.add_marriage(self,spouse,[],0)
			d.add_subgraph([self,spouse])


	for pID in pIDs:
		if pID==0:
			continue
		an(pID)
		ae((sID,pID))


	if eU.isKing(self):
		kings.append(sID)
		for spID in spIDs:
			if int(spID)!=0:
				kingsp.append(int(spID))
		
filename = '../familyTree/graphedData'
file = open(filename,'w')
pickle.dump(g,file)
file.close()

if everyone==0:
	gQ.read_graph()


	myNodes = gQ.join_up(kings,kings)
	myNodes+=gQ.join_up(kingsp,kings)
	print set(myNodes).difference(set(kings))

	#include people ancestors of wives of kings?

	myNodes = set(myNodes)
	myNodes = list(myNodes)
	for sID in myNodes:
		print sID
		self,sID,sN = aQ.find_person(sID)
		parents,pIDs,pNs = aQ.find_parents(sID)
		spouses,spIDs,spNs,spDs = aQ.find_spouses(sID)
	
		d.add_person(self)
#		if pIDs[0]!=0 or pIDs[1]!=0:
		if int(pIDs[0]) in myNodes or int(pIDs[1]) in myNodes:
			d.add_marriage(parents[0],parents[1],[self],0)
			d.add_subgraph(parents)

		else:
			print pIDs

		for spouse in spouses:
			d.add_marriage(self,spouse,[],0)
			d.add_subgraph([self,spouse])
print myNodes	
print 'create'
d.create_dot()
print 'sub'
d.add_subgraphs()
print 'end'
d.end_dot()
print 'render'
file = '/home/naath/public-html/bigGraph.png'
d.render_to_file(file)		

aQ.close()


