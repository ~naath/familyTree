#!/usr/bin/python

import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion
import everyPage

[conn,form]=everyPage.top()
#cgitb.enable()

age = form.getvalue('age')

if age  == None:
        printMe = askQuestion.count_age_at_death('<br>')
        if len(printMe)<3:
                printMe =  'sorry, no data <br>'

        everyPage.good(printMe)

else:
	result = re.match('[-]{0,1}[0-9]{1,7}$', str(age))
	if result==None:
		everyPage.bad()
	else:

		printMe  = askQuestion.people_died_at_age(age,'<br>')
        	if len(printMe)<10:
                	printMe =  'sorry, no data <br>'

	        everyPage.good(printMe)

everyPage.bottom()

