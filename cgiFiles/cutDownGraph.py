#!/usr/bin/python

import cgi
#import cgitb
import sys
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion as aQ
import make_dot as d

def add_quotes(s):
        s = str(s)
        return '\"'+s+'\"'

#cgitb.enable()

form = cgi.FieldStorage()
conn = aQ.connect()

famTree = aQ.list_people_parents()

d.start_dot(8)
for i in range(len(famTree)):
	self = famTree[i][0]

	addMe = 0

	selfID =  int(self.split(',')[-1])
	c = aQ.find_children(selfID)[1]

	if aQ.isKing(self)==1:
		addMe = 1
	else:
		children = aQ.find_children(selfID)[1]
		if len(children)>0:
			addMe = 1


	if addMe == 1:

		d.add_person(self)

		ps = []
		for j in range(len(famTree[i][1])):
			p = famTree[i][1][j]
			ps.append(str(p))

		if len(ps)==2:
			d.add_marriage(ps[0],ps[1],[self],0)
			d.add_subgraph(ps)


		for j in range(len(famTree[i][2])):
        	        s = famTree[i][2][j]
                	ps = [self,s]

                	d.add_marriage(self,s,[],0)
                	d.add_subgraph(ps)

d.add_subgraphs()
d.end_dot()
d.render_to_file()		

aQ.close(conn)
