#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
import cgi
#import cgitb
import sys
import re
sys.path.append('/home/naath/familyTreeProject/familyTree')
import askQuestion as aQ
import englishUtils as eU
import everyPage

def make_search_box(text,value):

	if value is None:
		value = ''
	sb = "%s:<input type = 'text' name='%s' value = %s><br>"\
		%(text,text,value)

	return sb
def make_int(value):
	if eU.is_number(value):
		return int(value)
	else:
		return 0

#cgitb.enable()

[conn,form]=everyPage.top()

year = form.getvalue('year')
month = form.getvalue('month')
if month is None:
	month = 0
day = form.getvalue('day')
if day is None:
	day = 0
search = "Search for people alive on given date<br>"\
+"<form id='searchBox' action = 'aliveOn.py' method = 'get'>"\
+make_search_box('year',year)\
+make_search_box('month',month)\
+make_search_box('day',day)\
+"<button type = 'submit'>Search</button><br><br>"\
+"</form>"

thisName=''
oneSearch = "Search for people by name"+\
"<form id='searchBox' action = 'aliveOn.py' method = 'get'>"+\
"Search for: <input type = 'text' name = 'name' value =" +thisName+"> <br>"+\
"<button type = 'submit'>Search</button><br><br>"+\
"</form>"


if year == None:
	printMe = search
	
	everyPage.good(printMe)	

else:
	year = make_int(year)
	month = make_int(month)
	day = make_int(day)

	if day ==0 and month !=0:
		printMe = 'start of %s %d'\
		%(eU.month_numbers(month),year)
	
	elif month == 0:
		printMe = 'start of %d'%(year)

	else:
		printMe = '%s of  %s  %d' \
		%(eU.ordinal_numbers(day),eU.month_numbers(month),year)


	printMe+='<br>'+aQ.alive_on(day,month,year,'<br>')

	printMe +='<br><br>'+search
	everyPage.good(printMe)



everyPage.bottom()

