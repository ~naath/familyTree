#!/usr/bin/python

import cgi
#import cgitb

import pygraph
from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.searching import breadth_first_search
from pygraph.readwrite.dot import write
import gv
import StringIO



def make_graph(Self,parents,children,otherparents):
	# Graph creation
	gr = digraph()

	# Add nodes and edges
	gr.add_nodes([Self])

	for p in parents:
		gr.add_nodes([p])
		gr.add_edge((p,Self))

	for i in range(len(children)):
		gr.add_nodes([children[i]])
		gr.add_edge((Self,children[i]))
		if not gr.has_node(otherparents[i]):
			gr.add_nodes([otherparents[i]])
		gr.add_edge((otherparents[i],children[i]))




	 #Draw as PNG
#	dot = write(gr)
#	gvv = gv.readstring(dot)
#	gv.layout(gvv,'dot')

#	print "Content-type: image/png\n"
#	print gv.render(gvv,'png')

form = cgi.FieldStorage()

Self = 'naath'
p = ['Bob','Susan']
c = ['Jane','Alice']
op = ['Mary','Mary']

make_graph(Self,p,c,op)
